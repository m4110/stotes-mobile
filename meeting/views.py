from django.shortcuts import render
from django.core import serializers
from django.core.serializers import serialize
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from .models import Meeting

import json
import datetime

@csrf_exempt
def deleteMeeting(request, id):
    if request.method == "DELETE":
        meeting = Meeting.objects.get(id=id)
        
        if meeting is None:
            return HttpResponse("Not found", status=404)

        meeting.delete()
        return HttpResponse("Deleted", status=200)

        
@csrf_exempt
def editMeeting(request, id):
    context = {}
    meeting = Meeting.objects.filter(id = id)
    context["meeting"] = serializers.serialize("json", meeting)
    context["meetingParticipants"] = serializers.serialize("json", meeting[0].meetingParticipants.all())
    
    
    project = meeting[0].project
    context["participants"] =  serializers.serialize("json", project.projectParticipants.all())
    
    if request.method == "POST":
        body = json.loads(request.body.decode("utf-8"))
        
        name = body["name"]
        description = body["description"]
        link = body["link"]
        dateTimeString = [int(string) for string in body["dateTime"].split(" ")[0].split("-")]
        dateTime = datetime.datetime(dateTimeString[0], dateTimeString[1], dateTimeString[2], int(body["dateTime"][11:13]), int(body["dateTime"][14:16]), int(body["dateTime"][17:19]))

        participantIds = [int(id) for id in json.loads(body["participants"])]
        meetingParticipants = [User.objects.get(id=id) for id in participantIds]

        meeting[0].name = name
        meeting[0].description = description
        meeting[0].link = link
        meeting[0].dateAndTime = dateTime
        meeting[0].save()

        for user in set(meetingParticipants):
            meeting[0].meetingParticipants.add(user)
        
        return HttpResponse("Updated", status=204)


    return JsonResponse(context, safe=False)

def currentMeeting(request, id):
    context = {}

    meeting = Meeting.objects.filter(id = id)
    context["chosenMeeting"] = serializers.serialize("json", meeting)
    listPeople = meeting[0].meetingParticipants.all()[:3]
    meetingParticipants = serializers.serialize("json", listPeople)
    context["meetingParticipants"] = meetingParticipants

    return JsonResponse(context, safe=False)

