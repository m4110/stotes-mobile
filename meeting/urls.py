from django.urls import path
from .views import deleteMeeting, editMeeting, currentMeeting

urlpatterns = [
    path("delete/<int:id>/", deleteMeeting, name="deleteMeeting"),
    path("edit/<int:id>/", editMeeting, name="editMeeting"),
    path("<int:id>", currentMeeting, name="currentMeeting")
]