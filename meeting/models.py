from django.db import models
from project.models import Project
from django.contrib.auth.models import User
from django.urls import reverse_lazy

# Create your models here.
class Meeting(models.Model):
    name = models.CharField(max_length = 30)
    dateAndTime = models.DateTimeField()
    description = models.TextField()
    link = models.URLField()

    project = models.ForeignKey(Project, on_delete = models.CASCADE, related_name = "meeting")
    creator = models.ForeignKey(User, on_delete = models.CASCADE, related_name = "meeting_creator")
    meetingParticipants = models.ManyToManyField(User, related_name = "meetings")

    def get_absolute_url(self):
        return reverse_lazy('edit_meeting', kwargs={'meeting_id': self.id})

    def __str__(self):
        return self.name