import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobileapp/screen/Home.dart';
import "package:mobileapp/screen/account/login.dart";
import "package:mobileapp/widgets/smallwidgets.dart";

import "package:shared_preferences/shared_preferences.dart";

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stotes',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.brown,
      ),
      home: HomePage('Stotes'),
    );
  }
}

Future<String> getSessionId() async {
  final preferences = await SharedPreferences.getInstance();
  return preferences.getString("sessionId") ?? "";
}

class HomePage extends StatefulWidget {
  final String title;

  HomePage(this.title);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late Future<String> sessionId;

  @override
  void initState() {
    super.initState();
    sessionId = getSessionId();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String>(
        future: sessionId,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            String? data = snapshot.data;
            return (data == "") ? LoginScreen() : StotesHome();
          }
          return LoadingIndicator();
        });
  }
}
