import 'dart:async';

class Newsfeed {
  final String title;
  final String summary;
  final String link;

  Newsfeed(this.title, this.summary, this.link);

  factory Newsfeed.fromJson(Map<String, dynamic> json) {
    return Newsfeed(json["title"], json["summary"], json["link"]);
  }
}

class Project {
  final int id;
  final String name;
  final String description;
  final String passcode;
  final DateTime startDate;
  final DateTime endDate;
  final int creatorId;
  final DateTime lastOpened;

  Project(
      {required this.id,
      required this.name,
      required this.description,
      required this.passcode,
      required this.startDate,
      required this.endDate,
      required this.creatorId,
      required this.lastOpened});

  factory Project.fromMap(Map<String, dynamic> json) {
    return Project(
      id: json["pk"] ?? 0,
      name: json["fields"]["name"],
      description: json["fields"]["description"],
      passcode: json["fields"]["passcode"],
      startDate: DateTime.parse(json["fields"]["startDate"]),
      endDate: DateTime.parse(json["fields"]["endDate"]),
      creatorId: json["fields"]["creator"] ?? 0,
      lastOpened: DateTime.parse(json["fields"]["lastOpened"]),
    );
  }


  Map<String, dynamic> toMap() {
    return {
      "pk": id,
      "fields": {
        "name": name,
        "description": description,
        "passcode": passcode,
        "startDate": startDate.toString(),
        "endDate": endDate.toString(),
        "creatorId": creatorId,
        "lastOpened": lastOpened.toString()
      }
    };
  }
}

class Task {
  final int id;
  final String name;
  final String description;
  final String type;
  final DateTime dueDate;
  final int projectId;

  Task({
    required this.id,
    required this.name,
    required this.description,
    required this.type,
    required this.dueDate,
    required this.projectId,
  });

  factory Task.fromMap(Map<String, dynamic> json) {
    return Task(
      id: json["pk"],
      name: json["fields"]["name"],
      description: json["fields"]["description"],
      type: json["fields"]["taskType"],
      dueDate: DateTime.parse(json["fields"]["dueDate"]),
      projectId: json["fields"]["project"],
    );
  }

  int getRemainingDays() {
    Duration difference = dueDate.difference(DateTime.now());
    return difference.inDays;
  }
}

class Meeting {
  final int id;
  final String name;
  final String description;
  final String link;
  final DateTime dateTime;
  final int projectId;
  final int creatorId;

  Meeting({
    required this.id,
    required this.name,
    required this.description,
    required this.link,
    required this.dateTime,
    required this.projectId,
    required this.creatorId,
  });

  factory Meeting.fromMap(Map<String, dynamic> json) {
    return Meeting(
      id: json["pk"],
      name: json["fields"]["name"],
      description: json["fields"]["description"],
      link: json["fields"]["link"],
      dateTime: DateTime.parse(json["fields"]["dateAndTime"]),
      projectId: json["fields"]["project"],
      creatorId: json["fields"]["creator"],
    );
  }

  int getRemainingHours() {
    Duration difference = dateTime.difference(DateTime.now());
    return difference.inHours;
  }
}

class User {
  final int id;
  final String username;
  final String? firstName;
  final String? lastName;
  final String? email;

  User({
    required this.id,
    required this.username,
    required this.firstName,
    required this.lastName,
    required this.email,
  });

  factory User.fromMap(Map<String, dynamic> json) {
    return User(
      id: json["pk"],
      username: json["fields"]["username"],
      firstName: json["fields"]["firstname"],
      lastName: json["fields"]["lastname"],
      email: json["fields"]["email"],
    );
  }

  @override
  String toString() {
    return username;
  }
}
