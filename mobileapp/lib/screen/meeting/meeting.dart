import "package:flutter/material.dart";
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:google_fonts/google_fonts.dart';
import "package:mobileapp/production.dart";
import 'package:mobileapp/screen/project/specific_projects.dart';
import 'package:mobileapp/widgets/boards.dart';
import 'package:mobileapp/widgets/cards.dart';
import "package:mobileapp/widgets/navigation.dart";
import "package:mobileapp/widgets/smallwidgets.dart";
import "package:mobileapp/widgets/form.dart";
import "package:mobileapp/model/models.dart";
import "package:http/http.dart" as http;
import "dart:convert";
import "dart:async";
import "package:mobileapp/screen/meeting/edit_meeting.dart";
import "package:mobileapp/production.dart";
import 'package:url_launcher/url_launcher.dart';

Future<Meeting> fetchCurrentMeeting(int pk) async {
  final response = await http.get(Uri.parse("${fetchUrl}meeting/$pk"));
  final meeting = json.decode(json.decode(response.body)["chosenMeeting"]);

  return Meeting.fromMap(meeting[0]);
}

Future<List<User>> fetchParticipants(int pk) async {
  final response = await http.get(Uri.parse("${fetchUrl}meeting/$pk"));
  final allParticipant =
      json.decode(json.decode(response.body)["meetingParticipants"]);

  return allParticipant.map<User>((user) => User.fromMap(user)).toList();
}

Future<bool> deleteMeeting(int meetingId) async {
  final response = await http.delete(
    Uri.parse("${fetchUrl}meeting/delete/$meetingId/"),
  );

  if (response.statusCode == 200) {
    return true;
  }

  return false;
}

class MeetingScreen extends StatefulWidget {
  final int pk;
  const MeetingScreen({Key? key, required this.pk}) : super(key: key);

  @override
  _MeetingScreenState createState() => _MeetingScreenState();
}

class _MeetingScreenState extends State<MeetingScreen> {
  late Future<Meeting> currentMeeting;
  late Future<List<User>> allParticipant;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    currentMeeting = fetchCurrentMeeting(widget.pk);
    allParticipant = fetchParticipants(widget.pk);
  }

  Future<void> refreshProject() async {
    setState(() {
      currentMeeting = fetchCurrentMeeting(widget.pk);
      allParticipant = fetchParticipants(widget.pk);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: Navbar(),
        drawer: NavDrawer(),
        backgroundColor: const Color.fromRGBO(255, 250, 242, 1),
        body: RefreshIndicator(
          onRefresh: () => refreshProject(),
          child: FutureBuilder<Meeting>(
              future: currentMeeting,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done &&
                    snapshot.hasData) {
                  Meeting meeting = snapshot.data!;
                  int remainingHours = meeting.getRemainingHours();
                  return Column(
                    children: [
                      Expanded(
                          child: ListView(
                        children: [
                          Heading(meeting.name, 30.0),
                          PaddingContainer(
                            SingleChildScrollView(
                                child: SizedBox(
                                    child: RubikText(
                                      meeting.description,
                                      16.0,
                                      textAlign: TextAlign.justify,
                                    ),
                                    height: MediaQuery.of(context).size.height *
                                        0.2)),
                          ),
                          PaddingContainer(
                            Row(
                              children: [
                                const Icon(
                                  Icons.calendar_today,
                                  color: Color.fromRGBO(52, 30, 11, 1),
                                  size: 19.5,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    meeting.dateTime
                                        .toString()
                                        .substring(0, 16),
                                    style: GoogleFonts.rubik(
                                      fontSize: 17.5,
                                      color:
                                          const Color.fromRGBO(52, 30, 11, 1),
                                      fontWeight: FontWeight.w600,
                                      shadows: <Shadow>[
                                        const Shadow(
                                          offset: Offset(2, 2),
                                          blurRadius: 15.0,
                                          color: Color.fromRGBO(
                                              120, 120, 120, 0.15),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          PaddingContainer(
                            Row(
                              children: [
                                const Icon(
                                  Icons.alarm,
                                  color: Color.fromRGBO(52, 30, 11, 1),
                                  size: 19.5,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(
                                    remainingHours > 0
                                        ? remainingHours > 24
                                            ? "Starts in " +
                                                (remainingHours / 24)
                                                    .floor()
                                                    .toString() +
                                                ((remainingHours / 24)
                                                            .floor() ==
                                                        1
                                                    ? " day"
                                                    : " days")
                                            : "Starts in $remainingHours hours"
                                        : "Started " +
                                            remainingHours.abs().toString() +
                                            " hours ago",
                                    style: GoogleFonts.rubik(
                                      fontSize: 17.5,
                                      color:
                                          const Color.fromRGBO(52, 30, 11, 1),
                                      fontWeight: FontWeight.w600,
                                      shadows: <Shadow>[
                                        const Shadow(
                                          offset: Offset(2, 2),
                                          blurRadius: 15.0,
                                          color: Color.fromRGBO(
                                              120, 120, 120, 0.15),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Heading("Participants", 20),
                          PaddingContainer(
                            FutureBuilder<List<User>>(
                              future: allParticipant,
                              builder: (context, snapshot) {
                                if (snapshot.connectionState ==
                                        ConnectionState.done &&
                                    snapshot.hasData) {
                                  return Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: snapshot.data!
                                        .map((user) =>
                                            DetailBlack(user.username))
                                        .toList(),
                                  );
                                } else if (snapshot.hasError) {
                                  return ErrorCard();
                                }
                                return LoadingIndicator();
                              },
                            ),
                          ),
                          const Divider(
                            thickness: 1.0,
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: ButtonGreen(
                              const ButtonText(
                                text: "Join meeting",
                              ),
                              () => launch(meeting.link),
                            ),
                          ),
                        ],
                      )),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Align(
                              child: SizedBox(
                                child: ButtonBeige(
                                    const ButtonText(
                                      text: "Back",
                                      color: Color.fromRGBO(52, 30, 11, 1),
                                      fontSize: 14.0,
                                    ), () {
                                  Navigator.pop(context);
                                }),
                                height: 30,
                              ),
                              alignment: Alignment.topCenter,
                            ),
                            Align(
                              child: SizedBox(
                                child: ButtonGreen(
                                    const ButtonText(
                                      text: "Edit Meeting",
                                      color: Colors.white,
                                      fontSize: 14.0,
                                    ), () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          EditMeetingScreen(id: meeting.id),
                                    ),
                                  );
                                }),
                                height: 30,
                              ),
                              alignment: Alignment.topCenter,
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              child: Align(
                                child: SizedBox(
                                  child: ButtonRed(
                                      child: const ButtonText(
                                        text: "Delete Meeting",
                                        color: Colors.white,
                                        fontSize: 14.0,
                                      ),
                                      callback: () {
                                        showDialog(
                                          context: context,
                                          barrierDismissible: false,
                                          builder: (context) => AlertDialog(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        10.0)),
                                            backgroundColor: Colors.orange[50],
                                            titlePadding:
                                                const EdgeInsets.all(0),
                                            title: Container(
                                              decoration: const BoxDecoration(
                                                color: Color.fromRGBO(
                                                    52, 30, 11, 1),
                                                borderRadius: BorderRadius.only(
                                                  topLeft:
                                                      Radius.circular(10.0),
                                                  topRight:
                                                      Radius.circular(10.0),
                                                ),
                                              ),
                                              child: const Text(
                                                "Delete meeting?",
                                                style: TextStyle(
                                                  fontFamily: "MADESunflower",
                                                  fontSize: 28.0,
                                                  color: Colors.white,
                                                ),
                                              ),
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                horizontal: 24.0,
                                                vertical: 12.0,
                                              ),
                                              // color: const Color.fromRGBO(52, 30, 11, 1),
                                            ),
                                            content: RichText(
                                                text: TextSpan(
                                              text: "Do you want to delete ",
                                              style: GoogleFonts.rubik(
                                                fontSize: 16,
                                                color: Colors.black,
                                              ),
                                              children: [
                                                TextSpan(
                                                  text: meeting.name,
                                                  style: GoogleFonts.rubik(
                                                    fontSize: 16,
                                                    fontWeight: FontWeight.w700,
                                                  ),
                                                ),
                                                const TextSpan(
                                                  text: " forever?",
                                                ),
                                              ],
                                            )),
                                            actions: [
                                              ButtonBeige(
                                                const ButtonText(
                                                  text: "Not this time.",
                                                  fontSize: 18.0,
                                                  color: Color.fromRGBO(
                                                      52, 30, 11, 1),
                                                ),
                                                () => Navigator.pop(context),
                                              ),
                                              ButtonRed(
                                                child: const ButtonText(
                                                  text: "Yep, delete.",
                                                  fontSize: 18,
                                                ),
                                                callback: () {
                                                  final deletion =
                                                      deleteMeeting(meeting.id);
                                                  deletion.then((status) {
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (_) =>
                                                                SpecificProject(
                                                                    pk: meeting
                                                                        .projectId)));
                                                  });
                                                },
                                              ),
                                            ],
                                          ),
                                        );
                                      }),
                                  height: 30,
                                ),
                                alignment: Alignment.topCenter,
                              ),
                            ),
                          ]),
                    ],
                  );
                } else if (snapshot.hasError) {
                  return ErrorCard();
                }
                return const LoadingIndicator();
              }),
        ));
  }
}
