import "package:flutter/material.dart";
import "package:mobileapp/production.dart";
import 'package:mobileapp/screen/project/specific_projects.dart';
import 'package:mobileapp/widgets/cards.dart';
import "package:mobileapp/widgets/navigation.dart";
import "package:mobileapp/widgets/smallwidgets.dart";
import "package:mobileapp/widgets/form.dart";
import "package:mobileapp/model/models.dart";
import "package:google_fonts/google_fonts.dart";
import "package:http/http.dart" as http;
import "dart:convert";
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

Future<List<User?>> getProjectParticipants(int projectId) async {
  final preferences = await SharedPreferences.getInstance();
  final sessionId = preferences.getString("sessionId");

  final response = await http.get(
    Uri.parse("${fetchUrl}project/$projectId/addMeeting/"),
    headers: {
      "sessionId": sessionId!,
    },
  );

  final result = json.decode(json.decode(response.body));
  final List<User?> participants = result
      .map<User?>(
        (user) => User.fromMap(user),
      )
      .toList();
  return participants;
}

class AddMeetingScreen extends StatefulWidget {
  const AddMeetingScreen({Key? key, required this.projectId}) : super(key: key);

  final int projectId;

  @override
  _AddMeetingScreenState createState() => _AddMeetingScreenState();
}

class _AddMeetingScreenState extends State<AddMeetingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Navbar(),
      body: SingleChildScrollView(
        child: AddMeetingForm(projectId: widget.projectId),
      ),
      backgroundColor: const Color.fromRGBO(255, 250, 242, 1),
    );
  }
}

class AddMeetingForm extends StatefulWidget {
  const AddMeetingForm({Key? key, required this.projectId}) : super(key: key);

  final int projectId;

  @override
  _AddMeetingFormState createState() => _AddMeetingFormState();
}

class _AddMeetingFormState extends State<AddMeetingForm> {
  @override
  final _formKey = GlobalKey<FormState>();

  final nameController = TextEditingController();
  final descriptionController = TextEditingController();
  final dateController = TextEditingController();
  final linkController = TextEditingController();

  Widget buttonContent = const ButtonText(text: "Create Meeting");

  List<int?> selectedUsers = [];

  late Future<List<User?>> participants;

  void startLoading() {
    setState(() {
      buttonContent = const ButtonLoadingScreen();
    });
  }

  void endLoading() {
    setState(() {
      buttonContent = const ButtonText(text: "Create Meeting");
    });
  }

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
    descriptionController.dispose();
    dateController.dispose();
    linkController.dispose();
  }

  @override
  void initState() {
    super.initState();
    participants = getProjectParticipants(widget.projectId);
    dateController.text = DateTime.now().toString();
  }

  Future<void> createMeeting(int projectId, String name, String description,
      String link, DateTime dateTime, List<int?> participants) async {
    Map<String, dynamic> rawProject = {
      "name": name,
      "description": description,
      "link": link,
      "dateTime": dateTime.toString(),
      "participants": json.encode(
        participants,
      ),
    };

    final preferences = await SharedPreferences.getInstance();
    final sessionId = preferences.getString("sessionId");

    final response = await http.post(
      Uri.parse("${fetchUrl}project/$projectId/addMeeting/"),
      headers: <String, String>{
        "Content-Type": "application/json; charset=UTF-8",
        "sessionId": sessionId!,
      },
      body: json.encode(rawProject),
    );

    if (response.statusCode == 201) {
      endLoading();
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (_) => SpecificProject(pk: widget.projectId),
        ),
      );
    } else {
      endLoading();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(
            "An error have occured.",
            style: GoogleFonts.rubik(
              color: const Color.fromRGBO(253, 248, 238, 1),
              fontSize: 14.0,
            ),
          ),
          backgroundColor: const Color.fromRGBO(125, 84, 49, 1),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<User?>>(
      future: participants,
      builder: (context, snapshot) {
        if (snapshot.hasData &&
            snapshot.connectionState == ConnectionState.done) {
          List<User?> projectParticipants = snapshot.data!;
          return Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.all(25.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Heading("Create Meeting", 44.0),
                    FormTextInput(
                      labelText: "Name",
                      isPassword: false,
                      controller: nameController,
                      validator: (value) => validateIfEmpty(value),
                    ),
                    FormTextInput(
                      labelText: "Link",
                      isPassword: false,
                      controller: linkController,
                      validator: (value) => validateUrl(value),
                    ),
                    FormTextArea(
                      name: "Description",
                      controller: descriptionController,
                    ),
                    RubikText(
                      "Date and Time: ",
                      18.0,
                    ),
                    TextButton(
                        onPressed: () {
                          DatePicker.showDateTimePicker(context,
                              showTitleActions: true,
                              minTime: DateTime.now(), onConfirm: (date) {
                            setState(() {
                              dateController.text = date.toString();
                            });
                          }, currentTime: DateTime.parse(dateController.text));
                        },
                        child: Text(
                          dateController.text.substring(0, 16),
                        )),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: UserMultipleSelect(
                        listOfUsers: projectParticipants,
                        heading: Text(
                          "Meeting Participants",
                          style: GoogleFonts.rubik(
                            color: Colors.white,
                            fontSize: 20.0,
                          ),
                        ),
                        onTap: (value) {
                          selectedUsers = value!;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ButtonBeige(
                            const ButtonText(
                                text: "Back",
                                color: Color.fromRGBO(52, 30, 11, 1)),
                            () => Navigator.pop(context),
                          ),
                          ButtonGreen(
                            buttonContent,
                            () {
                              if (_formKey.currentState!.validate()) {
                                _formKey.currentState!.save();
                                startLoading();
                                createMeeting(
                                  widget.projectId,
                                  nameController.text,
                                  descriptionController.text,
                                  linkController.text,
                                  DateTime.parse(dateController.text),
                                  selectedUsers,
                                );
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        } else if (snapshot.hasError) {
          return ErrorCard();
        }
        return const LoadingIndicator();
      },
    );
  }
}
