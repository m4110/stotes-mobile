import 'package:flutter/material.dart';
import 'package:mobileapp/model/models.dart';
import "package:http/http.dart" as http;
import 'package:mobileapp/widgets/cards.dart';
import 'package:mobileapp/widgets/boards.dart';
import 'package:mobileapp/widgets/navigation.dart';
import 'package:mobileapp/widgets/smallwidgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import "dart:convert";
import '../../production.dart';

Future<List<Meeting>> fetchMeetings() async {
  final preferences = await SharedPreferences.getInstance();
  String sessionId = preferences.getString("sessionId") ?? "";

  final response = await http.get(
    Uri.parse("${fetchUrl}my-meetings/"),
    headers: {
      "sessionId": sessionId,
    },
  );

  if (response.statusCode == 200) {
    final meetingData = json.decode(json.decode(response.body)["meetings"]);
    return meetingData.map<Meeting>((json) => Meeting.fromMap(json)).toList();
  } else {
    throw Exception("Failed to load meetings");
  }
}

class MyMeetings extends StatefulWidget {
  const MyMeetings({Key? key}) : super(key: key);

  @override
  _MyMeetingsState createState() => _MyMeetingsState();
}

class _MyMeetingsState extends State<MyMeetings> {
  late Future<List<Meeting>> futureMeetings;
  @override
  void initState() {
    super.initState();
    futureMeetings = fetchMeetings();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Navbar(),
      body: Container(
          child: ListView(
        children: [
          Heading("My Meetings", 30.0),
          PaddingContainer(
            RubikText(
              "Take a look at your upcoming meetings.",
              16.0,
              textAlign: TextAlign.justify,
            ),
          ),
          FutureBuilder<List<Meeting>>(
              future: futureMeetings,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done &&
                    snapshot.hasData) {
                  return MeetingBoard(
                    children: snapshot.data!,
                  );
                } else if (snapshot.hasError) {
                  return ErrorCard();
                }

                return LoadingIndicator();
              }),
        ],
      )),
      backgroundColor: const Color.fromRGBO(255, 250, 242, 1),
    );
  }
}
