import "package:flutter/material.dart";
import 'package:google_fonts/google_fonts.dart';
import 'package:mobileapp/screen/project/specific_projects.dart';
import 'package:mobileapp/widgets/cards.dart';

import "package:mobileapp/widgets/form.dart";
import "package:mobileapp/production.dart";
import "package:mobileapp/widgets/navigation.dart";
import "package:mobileapp/widgets/smallwidgets.dart";
import "package:mobileapp/model/models.dart";
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

import "package:http/http.dart" as http;
import "dart:convert";

Future<Meeting> getEditMeeting(int meetingId) async {
  final response = await http.get(
    Uri.parse("${fetchUrl}meeting/edit/$meetingId/"),
  );

  if (response.statusCode == 200) {
    final body = json.decode(json.decode(response.body)["meeting"])[0];
    final meeting = Meeting.fromMap(body);
    return meeting;
  } else {
    throw Exception("Failed to fetch meeting");
  }
}

Future<List<List<User?>>> getMeetingParticipants(int meetingId) async {
  final response = await http.get(
    Uri.parse("${fetchUrl}meeting/edit/$meetingId/"),
  );

  if (response.statusCode == 200) {
    final meetingParticipants = json
        .decode(json.decode(response.body)["meetingParticipants"])
        .map<User>(
          (user) => User.fromMap(user),
        )
        .toList();

    final participantJson =
        json.decode(json.decode(response.body)["participants"]);
    final participants = participantJson
        .map<User>(
          (user) => User.fromMap(user),
        )
        .toList();
    return [meetingParticipants, participants];
  } else {
    throw Exception("Failed to fetch participants");
  }
}

class EditMeetingScreen extends StatefulWidget {
  final int id;
  const EditMeetingScreen({Key? key, required this.id}) : super(key: key);

  @override
  _EditMeetingScreenState createState() => _EditMeetingScreenState();
}

class _EditMeetingScreenState extends State<EditMeetingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Navbar(),
      backgroundColor: const Color.fromRGBO(255, 250, 242, 1),
      body: SingleChildScrollView(
        child: EditMeetingForm(meetingId: widget.id),
      ),
    );
  }
}

class EditMeetingForm extends StatefulWidget {
  final int meetingId;
  const EditMeetingForm({Key? key, required this.meetingId}) : super(key: key);

  @override
  _EditMeetingFromState createState() => _EditMeetingFromState();
}

class _EditMeetingFromState extends State<EditMeetingForm> {
  final _formKey = GlobalKey<FormState>();
  late Future<Meeting> editedMeeting;
  late Future<List<List<User?>>> fetchParticipants;
  final nameController = TextEditingController();
  final descriptionController = TextEditingController();
  final dateController = TextEditingController();
  final linkController = TextEditingController();

  Widget buttonContent = const ButtonText(text: "Edit Meeting");

  @override
  void initState() {
    super.initState();
    editedMeeting = getEditMeeting(widget.meetingId);
    fetchParticipants = getMeetingParticipants(widget.meetingId);
  }

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
    descriptionController.dispose();
    dateController.dispose();
    linkController.dispose();
  }

  @override
  void startLoading() {
    setState(() {
      buttonContent = const ButtonLoadingScreen();
    });
  }

  Future<void> editMeeting(
      int taskId,
      int projectId,
      String name,
      String description,
      String link,
      DateTime dateTime,
      List<int?> participants) async {
    Map<String, dynamic> rawProject = {
      "name": name,
      "description": description,
      "link": link,
      "dateTime": dateTime.toString(),
      "participants": json.encode(
        participants,
      ),
    };
    final response = await http.post(
      Uri.parse("${fetchUrl}meeting/edit/${widget.meetingId}/"),
      body: json.encode(rawProject),
      headers: <String, String>{
        "Content-Type": "application/json; charset=UTF-8",
      },
    );

    if (response.statusCode == 204) {
      endLoading();
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (_) => SpecificProject(pk: projectId),
        ),
      );
    } else {
      endLoading();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(
            "An error have occured.",
            style: GoogleFonts.rubik(
              color: const Color.fromRGBO(253, 248, 238, 1),
              fontSize: 14.0,
            ),
          ),
          backgroundColor: const Color.fromRGBO(125, 84, 49, 1),
        ),
      );
    }
  }

  @override
  void endLoading() {
    setState(() {
      buttonContent = const ButtonText(text: "Edit Meeting");
    });
  }

  List<int?> selectedUsers = [];

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Meeting>(
        future: editedMeeting,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.hasData) {
            Meeting fetchedMeeting = snapshot.data!;
            if (nameController.text == '') {
              nameController.text = fetchedMeeting.name;
            }
            if (descriptionController.text == '') {
              descriptionController.text = fetchedMeeting.description;
            }
            if (linkController.text == '') {
              linkController.text = fetchedMeeting.link;
            }
            if (dateController.text == '') {
              dateController.text = fetchedMeeting.dateTime.toString();
            }
            return Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.all(25.0),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Heading("Edit Meeting", 44.0),
                      FormTextInput(
                        labelText: "Name",
                        isPassword: false,
                        controller: nameController,
                        validator: (value) => validateIfEmpty(value),
                      ),
                      FormTextInput(
                        labelText: "Link",
                        isPassword: false,
                        controller: linkController,
                        validator: (value) => validateUrl(value),
                      ),
                      FormTextArea(
                        name: "Description",
                        controller: descriptionController,
                      ),
                      RubikText(
                        "Date and Time: ",
                        18.0,
                      ),
                      TextButton(
                          onPressed: () {
                            DatePicker.showDateTimePicker(context,
                                showTitleActions: true,
                                minTime: DateTime.now(), onConfirm: (date) {
                              setState(() {
                                dateController.text = date.toString();
                              });
                            },
                                currentTime:
                                    DateTime.parse(dateController.text));
                          },
                          child: Text(
                            dateController.text.substring(0, 16),
                          )),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: FutureBuilder<List<List<User?>>>(
                            future: fetchParticipants,
                            builder: (context, snapshot) {
                              if (snapshot.connectionState ==
                                      ConnectionState.done &&
                                  snapshot.hasData) {
                                List<User?> meetingParticipants =
                                    snapshot.data![0];
                                List<User?> projectParticipants =
                                    snapshot.data![1];
                                return UserMultipleSelect(
                                  listOfUsers: projectParticipants,
                                  heading: Text(
                                    "Meeting Participants",
                                    style: GoogleFonts.rubik(
                                      color: Colors.white,
                                      fontSize: 20.0,
                                    ),
                                  ),
                                  userValues: meetingParticipants
                                      .map<int>(
                                        (user) => user!.id,
                                      )
                                      .toList(),
                                  onTap: (value) {
                                    selectedUsers = value!;
                                  },
                                );
                              } else if (snapshot.hasError) {
                                return ErrorCard();
                              }
                              return const LoadingIndicator();
                            }),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ButtonBeige(
                              const ButtonText(
                                  text: "Back",
                                  color: Color.fromRGBO(52, 30, 11, 1)),
                              () => Navigator.pop(context),
                            ),
                            ButtonGreen(
                              buttonContent,
                              () {
                                if (_formKey.currentState!.validate()) {
                                  _formKey.currentState!.save();
                                  startLoading();
                                  editMeeting(
                                    widget.meetingId,
                                    fetchedMeeting.projectId,
                                    nameController.text,
                                    descriptionController.text,
                                    linkController.text,
                                    DateTime.parse(dateController.text),
                                    selectedUsers,
                                  );
                                }
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          } else if (snapshot.hasError) {
            return ErrorCard();
          }
          return const LoadingIndicator();
        });
  }
}
