import "package:flutter/material.dart";
import "package:mobileapp/production.dart";
import 'package:mobileapp/screen/project/specific_projects.dart';
import 'package:mobileapp/widgets/cards.dart';
import "package:mobileapp/widgets/navigation.dart";
import "package:mobileapp/widgets/smallwidgets.dart";
import "package:mobileapp/widgets/form.dart";
import "package:mobileapp/model/models.dart";
import "package:google_fonts/google_fonts.dart";
import "package:http/http.dart" as http;
import "dart:convert";
import 'package:shared_preferences/shared_preferences.dart';

Future<List<User?>> getProjectParticipants(int projectId) async {
  final preferences = await SharedPreferences.getInstance();
  final sessionId = preferences.getString("sessionId");

  final response = await http.get(
    Uri.parse("${fetchUrl}project/$projectId/addTask/"),
    headers: {
      "sessionId": sessionId!,
    },
  );

  final result = json.decode(json.decode(response.body));
  final List<User?> participants = result
      .map<User?>(
        (user) => User.fromMap(user),
      )
      .toList();
  return participants;
}

class AddTaskScreen extends StatefulWidget {
  const AddTaskScreen({Key? key, required this.projectId}) : super(key: key);

  final int projectId;

  @override
  _AddTaskScreenState createState() => _AddTaskScreenState();
}

class _AddTaskScreenState extends State<AddTaskScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Navbar(),
      body: SingleChildScrollView(
        child: AddTaskForm(projectId: widget.projectId),
      ),
      backgroundColor: const Color.fromRGBO(255, 250, 242, 1),
    );
  }
}

class AddTaskForm extends StatefulWidget {
  const AddTaskForm({Key? key, required this.projectId}) : super(key: key);

  final int projectId;

  @override
  _AddTaskFormState createState() => _AddTaskFormState();
}

class _AddTaskFormState extends State<AddTaskForm> {
  @override
  final _formKey = GlobalKey<FormState>();

  final nameController = TextEditingController();
  final descriptionController = TextEditingController();
  final dateController = TextEditingController();
  final dropdownMenu = DropdownMenu(
    dropdowns: const <String>[
      "Presentation",
      "Report",
      "Design",
      "Social Media",
      "Assignment",
      "Paper",
    ],
    initValue: "Presentation",
  );

  Widget buttonContent = const ButtonText(text: "Create Project");

  List<int?> selectedUsers = [];

  late Future<List<User?>> participants;

  void startLoading() {
    setState(() {
      buttonContent = const ButtonLoadingScreen();
    });
  }

  void endLoading() {
    setState(() {
      buttonContent = const ButtonText(text: "Create Project");
    });
  }

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
    descriptionController.dispose();
    dateController.dispose();
  }

  @override
  void initState() {
    super.initState();
    participants = getProjectParticipants(widget.projectId);
  }

  Future<void> createTask(int projectId, String name, String description,
      String type, DateTime duedate, List<int?> participants) async {
    Map<String, dynamic> rawProject = {
      "name": name,
      "description": description,
      "type": type,
      "dueDate": duedate.toString(),
      "participants": json.encode(
        participants,
      ),
    };

    final preferences = await SharedPreferences.getInstance();
    final sessionId = preferences.getString("sessionId");

    final response = await http.post(
      Uri.parse("${fetchUrl}project/$projectId/addTask/"),
      headers: <String, String>{
        "Content-Type": "application/json; charset=UTF-8",
        "sessionId": sessionId!,
      },
      body: json.encode(rawProject),
    );

    if (response.statusCode == 201) {
      endLoading();
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (_) => SpecificProject(pk: widget.projectId),
        ),
      );
    } else {
      endLoading();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(
            "An error have occured.",
            style: GoogleFonts.rubik(
              color: const Color.fromRGBO(253, 248, 238, 1),
              fontSize: 14.0,
            ),
          ),
          backgroundColor: const Color.fromRGBO(125, 84, 49, 1),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<User?>>(
      future: participants,
      builder: (context, snapshot) {
        if (snapshot.hasData &&
            snapshot.connectionState == ConnectionState.done) {
          List<User?> projectParticipants = snapshot.data!;
          return Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.all(25.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Heading("Create Task", 48.0),
                    FormTextInput(
                      labelText: "Name",
                      isPassword: false,
                      controller: nameController,
                      validator: (value) => validateIfEmpty(value),
                    ),
                    FormTextArea(
                      name: "Description",
                      controller: descriptionController,
                    ),
                    RubikText(
                      "Task types: ",
                      18.0,
                    ),
                    dropdownMenu,
                    RubikText(
                      "Due Date: ",
                      18.0,
                    ),
                    DatePickerButton(
                      controller: dateController,
                      initDate: DateTime.now(),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: UserMultipleSelect(
                        listOfUsers: projectParticipants,
                        heading: Text(
                          "People in Charge",
                          style: GoogleFonts.rubik(
                            color: Colors.white,
                            fontSize: 20.0,
                          ),
                        ),
                        onTap: (value) {
                          selectedUsers = value!;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ButtonBeige(
                            const ButtonText(
                                text: "Back",
                                color: Color.fromRGBO(52, 30, 11, 1)),
                            () => Navigator.pop(context),
                          ),
                          ButtonGreen(
                            buttonContent,
                            () {
                              if (_formKey.currentState!.validate()) {
                                _formKey.currentState!.save();
                                startLoading();
                                createTask(
                                  widget.projectId,
                                  nameController.text,
                                  descriptionController.text,
                                  dropdownMenu.value,
                                  DateTime.parse(dateController.text),
                                  selectedUsers,
                                );
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        } else if (snapshot.hasError) {
          return ErrorCard();
        }
        return const LoadingIndicator();
      },
    );
  }
}
