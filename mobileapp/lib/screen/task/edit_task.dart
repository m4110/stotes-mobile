import "package:flutter/material.dart";
import 'package:google_fonts/google_fonts.dart';
import 'package:mobileapp/screen/project/specific_projects.dart';
import 'package:mobileapp/widgets/cards.dart';

import "package:mobileapp/widgets/form.dart";
import "package:mobileapp/production.dart";
import "package:mobileapp/widgets/navigation.dart";
import "package:mobileapp/widgets/smallwidgets.dart";
import "package:mobileapp/model/models.dart";

import "package:http/http.dart" as http;
import "dart:convert";

Future<Task> getEditTask(int taskId) async {
  final response = await http.get(
    Uri.parse("${fetchUrl}task/edit/$taskId/"),
  );

  if (response.statusCode == 200) {
    final body = json.decode(json.decode(response.body)["task"])[0];
    final task = Task.fromMap(body);
    return task;
  } else {
    throw Exception("Failed to fetch task");
  }
}

Future<List<List<User?>>> getPeopleInCharge(int taskId) async {
  final response = await http.get(
    Uri.parse("${fetchUrl}task/edit/$taskId/"),
  );

  if (response.statusCode == 200) {
    final pic = json.decode(json.decode(response.body)["peopleInCharge"]);
    final peopleInCharge = pic
        .map<User>(
          (user) => User.fromMap(user),
        )
        .toList();

    final participantJson =
        json.decode(json.decode(response.body)["participants"]);
    final participants = participantJson
        .map<User>(
          (user) => User.fromMap(user),
        )
        .toList();
    return [peopleInCharge, participants];
  } else {
    throw Exception("Failed to fetch people in charge");
  }
}

class EditTaskScreen extends StatefulWidget {
  final int id;
  const EditTaskScreen({Key? key, required this.id}) : super(key: key);

  @override
  _EditTaskScreenState createState() => _EditTaskScreenState();
}

class _EditTaskScreenState extends State<EditTaskScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Navbar(),
      backgroundColor: const Color.fromRGBO(255, 250, 242, 1),
      body: SingleChildScrollView(
        child: EditTaskFrom(taskId: widget.id),
      ),
    );
  }
}

class EditTaskFrom extends StatefulWidget {
  final int taskId;
  const EditTaskFrom({Key? key, required this.taskId}) : super(key: key);

  @override
  _EditTaskFromState createState() => _EditTaskFromState();
}

class _EditTaskFromState extends State<EditTaskFrom> {
  final _formKey = GlobalKey<FormState>();
  late Future<Task> editedTask;
  late Future<List<List<User?>>> fetchParticipants;

  final nameController = TextEditingController();
  final descriptionController = TextEditingController();
  final dateController = TextEditingController();
  final dropdownMenu = DropdownMenu(
    dropdowns: const <String>[
      "Presentation",
      "Report",
      "Design",
      "Social Media",
      "Assignment",
      "Paper",
    ],
    initValue: "Presentation",
  );

  Widget buttonContent = const ButtonText(text: "Edit Task");

  @override
  void initState() {
    super.initState();
    editedTask = getEditTask(widget.taskId);
    fetchParticipants = getPeopleInCharge(widget.taskId);
  }

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
    descriptionController.dispose();
    dateController.dispose();
  }

  @override
  void startLoading() {
    setState(() {
      buttonContent = const ButtonLoadingScreen();
    });
  }

  Future<void> editTask(
      int taskId,
      int projectId,
      String name,
      String description,
      String type,
      DateTime duedate,
      List<int?> participants) async {
    Map<String, dynamic> rawProject = {
      "name": name,
      "description": description,
      "type": type,
      "dueDate": duedate.toString(),
      "participants": json.encode(
        participants,
      ),
    };
    final response = await http.post(
      Uri.parse("${fetchUrl}task/edit/${widget.taskId}/"),
      body: json.encode(rawProject),
      headers: <String, String>{
        "Content-Type": "application/json; charset=UTF-8",
      },
    );

    if (response.statusCode == 204) {
      endLoading();
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (_) => SpecificProject(pk: projectId),
        ),
      );
    } else {
      endLoading();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(
            "An error have occured.",
            style: GoogleFonts.rubik(
              color: const Color.fromRGBO(253, 248, 238, 1),
              fontSize: 14.0,
            ),
          ),
          backgroundColor: const Color.fromRGBO(125, 84, 49, 1),
        ),
      );
    }
  }

  @override
  void endLoading() {
    setState(() {
      buttonContent = const ButtonText(text: "Edit Task");
    });
  }

  List<int?> selectedUsers = [];

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Task>(
        future: editedTask,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.hasData) {
            Task fetchedTask = snapshot.data!;
            nameController.text = fetchedTask.name;
            descriptionController.text = fetchedTask.description;
            dropdownMenu.initValue = fetchedTask.type;
            return Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.all(25.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Heading("Edit task", 48.0),
                    FormTextInput(
                      labelText: "Name",
                      isPassword: false,
                      controller: nameController,
                      validator: (value) => validateIfEmpty(value),
                    ),
                    FormTextArea(
                      name: "Description",
                      controller: descriptionController,
                    ),
                    RubikText(
                      "Task Type: ",
                      18.0,
                    ),
                    dropdownMenu,
                    RubikText("Due Date: ", 18.0),
                    DatePickerButton(
                      controller: dateController,
                      initDate: fetchedTask.dueDate,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: FutureBuilder<List<List<User?>>>(
                          future: fetchParticipants,
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                    ConnectionState.done &&
                                snapshot.hasData) {
                              List<User?> peopleInCharge = snapshot.data![0];
                              List<User?> projectParticipants =
                                  snapshot.data![1];
                              return UserMultipleSelect(
                                listOfUsers: projectParticipants,
                                heading: Text(
                                  "People in Charge",
                                  style: GoogleFonts.rubik(
                                    color: Colors.white,
                                    fontSize: 20.0,
                                  ),
                                ),
                                userValues: peopleInCharge
                                    .map<int>(
                                      (user) => user!.id,
                                    )
                                    .toList(),
                                onTap: (value) {
                                  selectedUsers = value!;
                                },
                              );
                            } else if (snapshot.hasError) {
                              return ErrorCard();
                            }
                            return const LoadingIndicator();
                          }),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ButtonBeige(
                            const ButtonText(
                                text: "Back",
                                color: Color.fromRGBO(52, 30, 11, 1)),
                            () => Navigator.pop(context),
                          ),
                          ButtonGreen(
                            buttonContent,
                            () {
                              if (_formKey.currentState!.validate()) {
                                _formKey.currentState!.save();
                                startLoading();
                                editTask(
                                  widget.taskId,
                                  fetchedTask.projectId,
                                  nameController.text,
                                  descriptionController.text,
                                  dropdownMenu.value,
                                  DateTime.parse(dateController.text),
                                  selectedUsers,
                                );
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          } else if (snapshot.hasError) {
            return ErrorCard();
          }
          return const LoadingIndicator();
        });
  }
}
