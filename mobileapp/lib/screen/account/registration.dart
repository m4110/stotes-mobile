import "package:flutter/material.dart";

import "package:http/http.dart" as http;
import "package:google_fonts/google_fonts.dart";
import "dart:convert";

import "package:mobileapp/widgets/form.dart";
import "package:mobileapp/widgets/smallwidgets.dart";
import "package:mobileapp/production.dart";
import "package:mobileapp/screen/account/login.dart";

Future<String> createUser(
    String username, String name, String email, String password) async {
  final response = await http.post(
    Uri.parse("${fetchUrl}account/register/"),
    headers: <String, String>{
      "Content-Type": "application/json; charset=UTF-8",
    },
    body: jsonEncode(<String, String>{
      "username": username,
      "name": name,
      "email": email,
      "password": password,
    }),
  );

  if (response.statusCode == 201) {
    return "Success";
  } else if (response.statusCode == 409) {
    return response.body;
  } else {
    throw Exception("Error in creating user");
  }
}

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        child: Stack(
          children: <Widget>[
            Positioned(
              child: Container(
                  height: 300,
                  width: 300,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(200),
                    color: const Color.fromRGBO(203, 172, 129, 1),
                  )),
              bottom: -100,
              left: -100,
            ),
            Positioned(
              child: Container(
                  height: 150,
                  width: 150,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(200),
                    color: const Color.fromRGBO(137, 150, 86, 1),
                  )),
              bottom: -80,
              left: 120,
            ),
            Positioned(
              child: Container(
                  height: 300,
                  width: 300,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(150),
                    color: const Color.fromRGBO(137, 150, 86, 1),
                  )),
              bottom: MediaQuery.of(context).size.height * 0.3,
              right: -200,
            ),
            Positioned(
              child: Container(
                  height: 100,
                  width: 100,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: const Color.fromRGBO(203, 172, 129, 1),
                  )),
              bottom: MediaQuery.of(context).size.height * 0.58,
              right: 30,
            ),
            Positioned(
              child: Container(
                  height: 200,
                  width: 200,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                    color: const Color.fromRGBO(203, 172, 129, 1),
                  )),
              top: -100,
              left: 15,
            ),
            Positioned(
              child: Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: const Color.fromRGBO(125, 84, 49, 1),
                  )),
              top: 100,
              left: 15,
            ),
            Positioned(
              child: Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  height: MediaQuery.of(context).size.height * 0.9,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25.0),
                    color: const Color.fromRGBO(52, 30, 11, 0.1),
                  ),
                  child: const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                    child: RegisterFormColumn(),
                  )),
              top: MediaQuery.of(context).size.height * 0.1,
              left: MediaQuery.of(context).size.width * 0.1,
              right: MediaQuery.of(context).size.width * 0.1,
              bottom: MediaQuery.of(context).size.height * 0.1,
            ),
          ],
        ),
      ),
      backgroundColor: Colors.orange[50],
    );
  }
}

class RegisterFormColumn extends StatelessWidget {
  const RegisterFormColumn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Heading("Sign Up", 40.0),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: RubikText("Welcome to the Stotes family.", 18.0),
          ),
          RegisterForm(),
        ],
      ),
    );
  }
}

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key? key}) : super(key: key);

  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final _formKey = GlobalKey<FormState>();

  String passwordTemp = "";

  final usernameController = TextEditingController();
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmController = TextEditingController();

  Widget buttonContent = const ButtonText(text: "Sign up");

  @override
  void dipsose() {
    usernameController.dispose();
    nameController.dispose();
    emailController.dispose();
    passwordController.dispose();
    confirmController.dispose();
    super.dispose();
  }

  void startLoading() {
    setState(() {
      buttonContent = ButtonLoadingScreen();
    });
  }

  void endLoading() {
    setState(() {
      buttonContent = const ButtonText(text: "Sign up");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 15.0),
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            FormTextInput(
              labelText: "Username",
              isPassword: false,
              controller: usernameController,
              validator: (value) => validateIfEmpty(value),
            ),
            FormTextInput(
              labelText: "Name",
              isPassword: false,
              controller: nameController,
              validator: (value) => validateIfEmpty(value),
            ),
            FormTextInput(
              labelText: "Email",
              isPassword: false,
              controller: emailController,
              validator: (value) => validateEmail(value),
            ),
            FormTextInput(
              labelText: "Passsword",
              isPassword: true,
              controller: passwordController,
              validator: (value) {
                String error = validatePassword(value) ?? "";
                if (error != "") {
                  return error;
                }
                _formKey.currentState!.save();
              },
              onSaved: (value) {
                passwordTemp = passwordController.text;
              },
            ),
            FormTextInput(
              labelText: "Confirm Passsword",
              isPassword: true,
              controller: confirmController,
              validator: (value) {
                String error = validatePassword(value) ?? "";
                if (error.isNotEmpty) {
                  return error;
                } else if (passwordTemp != value) {
                  return "Your passwords do not match.";
                }
                return null;
              },
            ),
            ButtonGreenStretch(buttonContent, () {
              if (_formKey.currentState!.validate()) {
                startLoading();
                final userCreated = createUser(
                    usernameController.text,
                    nameController.text,
                    emailController.text,
                    passwordController.text);
                userCreated.then((value) {
                  if (value == "Duplicate User") {
                    endLoading();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        behavior: SnackBarBehavior.floating,
                        content: Text(
                          "The username already exists.",
                          style: GoogleFonts.rubik(
                            color: const Color.fromRGBO(253, 248, 238, 1),
                            fontSize: 14.0,
                          ),
                        ),
                        backgroundColor: const Color.fromRGBO(125, 84, 49, 1),
                      ),
                    );
                  } else if (value == "Duplicate Email") {
                    endLoading();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        behavior: SnackBarBehavior.floating,
                        content: Text(
                          "The email already exists.",
                          style: GoogleFonts.rubik(
                            color: const Color.fromRGBO(253, 248, 238, 1),
                            fontSize: 14.0,
                          ),
                        ),
                        backgroundColor: const Color.fromRGBO(125, 84, 49, 1),
                      ),
                    );
                  } else {
                    endLoading();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        behavior: SnackBarBehavior.floating,
                        content: Text(
                          "User has been created.",
                          style: GoogleFonts.rubik(
                            color: const Color.fromRGBO(253, 248, 238, 1),
                            fontSize: 14.0,
                          ),
                        ),
                        backgroundColor: const Color.fromRGBO(125, 84, 49, 1),
                      ),
                    );
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => LoginScreen(),
                      ),
                    );
                  }
                });
              }
            }),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RubikText(
                  "Have an account?",
                  14.0,
                ),
                TextButton(
                  child: Text(
                    "Login here.",
                    style: GoogleFonts.rubik(
                      color: const Color.fromRGBO(102, 108, 78, 1),
                      fontSize: 14.0,
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (_) => LoginScreen()),
                    );
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
