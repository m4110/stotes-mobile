import "package:flutter/material.dart";
import "package:mobileapp/production.dart";
import 'package:mobileapp/screen/account/edit_profile.dart';
import 'package:mobileapp/screen/account/login.dart';
import 'package:mobileapp/screen/project/specific_projects.dart';
import 'package:mobileapp/widgets/cards.dart';
import "package:mobileapp/widgets/navigation.dart";
import "package:mobileapp/widgets/smallwidgets.dart";
import "package:mobileapp/widgets/form.dart";
import "package:mobileapp/model/models.dart";
import "package:google_fonts/google_fonts.dart";
import "package:http/http.dart" as http;
import "dart:convert";
import 'package:shared_preferences/shared_preferences.dart';

Future<bool> logout() async {
  final preferences = await SharedPreferences.getInstance();
  final sessionId = preferences.getString("sessionId");

  final logoutRequest = await http.post(
    Uri.parse("${fetchUrl}account/logout/"),
    headers: <String, String>{
      "content-type": "application/json; charset=UTF-8",
    },
    body: json.encode({
      "sessionid": sessionId,
    }),
  );

  if (logoutRequest.statusCode == 200) {
    preferences.remove("sessionId");
    return true;
  }

  throw Exception("Failed to log out.");
}

Future<User> fetchCurrentUser() async {
  final preferences = await SharedPreferences.getInstance();
  String sessionId = preferences.getString("sessionId") ?? "";
  final response = await http.get(
    Uri.parse("${fetchUrl}account/getuser/"),
    headers: {
      "sessionId": sessionId,
    },
  );
  if (response.statusCode == 200) {
    final body = json.decode(response.body);
    final user = User(
      firstName: body["first_name"],
      lastName: body["last_name"],
      username: body["username"],
      id: body["id"],
      email: body["email"],
    );
    return user;
  } else {
    throw Exception("Failed to load current user");
  }
}

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  late Future<User> user;

  @override
  void initState() {
    super.initState();
    user = fetchCurrentUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Navbar(),
      body: FutureBuilder<User>(
          future: user,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                snapshot.hasData) {
              User fetchuser = snapshot.data!;
              return Column(
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.6,
                    child: Container(
                      margin: EdgeInsets.all(30.0),
                      padding: EdgeInsets.symmetric(vertical: 35),
                      decoration: BoxDecoration(
                        color: Colors.orange[50],
                        borderRadius: BorderRadius.circular(20.0),
                        border: Border.all(
                          color: const Color.fromRGBO(52, 30, 11, 1),
                          width: 2.0,
                        ),
                      ),
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          Heading(
                            "My Profile",
                            30.0,
                          ),
                          Image.asset(
                            "assets/images/happystote.png",
                            width: MediaQuery.of(context).size.width * 0.25,
                          ),
                          PaddingContainer(
                            RubikText(
                              "${fetchuser.firstName} ${fetchuser.lastName}",
                              22.0,
                              textAlign: TextAlign.justify,
                            ),
                          ),
                          PaddingContainer(
                            RubikText(
                              fetchuser.username,
                              22.0,
                              textAlign: TextAlign.justify,
                            ),
                          ),
                          PaddingContainer(
                            RubikText("My email : ${fetchuser.email}", 18.0),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ButtonGreen(
                        const ButtonText(text: "Edit Profile"),
                        () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => EditProfileScreen()),
                          );
                        },
                      ),
                      Align(
                        child: Padding(
                          child: ButtonBeige(
                              const ButtonText(
                                text: "Log Out",
                                color: Color.fromRGBO(52, 30, 11, 1),
                              ), () {
                            final loggedOut = logout();
                            loggedOut.then((isLoggedOut) {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => LoginScreen()),
                              );
                            });
                          }),
                          padding: EdgeInsets.symmetric(
                              horizontal:
                                  MediaQuery.of(context).size.width * 0.05),
                        ),
                        alignment: Alignment.topLeft,
                      ),
                    ],
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Center(
                child: ErrorCard(),
              );
            }
            return const Center(
              child: LoadingIndicator(),
            );
          }),
      drawer: NavDrawer(),
      backgroundColor: const Color.fromRGBO(255, 250, 242, 1),
    );
  }
}
