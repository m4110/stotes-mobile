import "package:flutter/material.dart";
import 'package:google_fonts/google_fonts.dart';
import "package:animated_text_kit/animated_text_kit.dart";
import "package:shared_preferences/shared_preferences.dart";
import "package:mobileapp/widgets/form.dart";
import "package:mobileapp/widgets/smallwidgets.dart";
import "package:mobileapp/production.dart";
import 'package:mobileapp/screen/Home.dart';
import "package:mobileapp/screen/account/registration.dart";

import "package:http/http.dart" as http;
import "dart:convert";

Future<String?> sendLoginInfo(String username, String password) async {
  final String url = "${fetchUrl}account/login/";
  final response = await http.post(
    Uri.parse(url),
    headers: <String, String>{
      "Content-Type": "application/json; charset=UTF-8",
    },
    body: json.encode(<String, String>{
      "username": username,
      "password": password,
    }),
  );

  if (response.statusCode == 200) {
    return json.decode(response.body)["sessionId"];
  } else if (response.statusCode == 401) {
    return null;
  } else {
    print(response.statusCode);
    throw Exception("Something went wrong in the login process");
  }
}

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
          child: Stack(
            children: <Widget>[
              Positioned(
                child: Container(
                  height: 300,
                  width: 300,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(200),
                    color: const Color.fromRGBO(203, 172, 129, 1),
                  ),
                ),
                bottom: -100,
                left: -100,
              ),
              Positioned(
                child: Container(
                    height: 150,
                    width: 150,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(200),
                      color: const Color.fromRGBO(137, 150, 86, 1),
                    )),
                bottom: -80,
                left: 120,
              ),
              Positioned(
                child: Container(
                    height: 300,
                    width: 300,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(150),
                      color: const Color.fromRGBO(137, 150, 86, 1),
                    )),
                bottom: MediaQuery.of(context).size.height * 0.3,
                right: -200,
              ),
              Positioned(
                child: Container(
                    height: 100,
                    width: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: const Color.fromRGBO(203, 172, 129, 1),
                    )),
                bottom: MediaQuery.of(context).size.height * 0.58,
                right: 30,
              ),
              Positioned(
                child: Container(
                    height: 200,
                    width: 200,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: const Color.fromRGBO(203, 172, 129, 1),
                    )),
                top: -100,
                left: 15,
              ),
              Positioned(
                child: Container(
                    height: 50,
                    width: 50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: const Color.fromRGBO(125, 84, 49, 1),
                    )),
                top: 100,
                left: 15,
              ),
              Positioned(
                child: Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    height: MediaQuery.of(context).size.height * 0.7,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25.0),
                      color: const Color.fromRGBO(52, 30, 11, 0.1),
                    ),
                    child: LoginForm()),
                top: MediaQuery.of(context).size.height * 0.15,
                left: MediaQuery.of(context).size.width * 0.1,
                right: MediaQuery.of(context).size.width * 0.1,
                bottom: MediaQuery.of(context).size.height * 0.15,
              ),
            ],
          ),
          color: Colors.orange[50]),
    );
  }
}

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(30.0),
      child: Column(
        children: [
          const RotatingText(),
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Text(
                "Log in to your Stotes.",
                style: GoogleFonts.rubik(
                  color: Color.fromRGBO(52, 30, 11, 1),
                  fontSize: 16.0,
                ),
              ),
            ),
          ),
          LoginFormFields(),
          Align(
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RubikText(
                  "Don't have an account?",
                  14.0,
                ),
                TextButton(
                  child: Text(
                    "Sign up here.",
                    style: GoogleFonts.rubik(
                      color: const Color.fromRGBO(102, 108, 78, 1),
                      fontSize: 14.0,
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (_) => RegistrationScreen()),
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class LoginFormFields extends StatefulWidget {
  const LoginFormFields({Key? key}) : super(key: key);

  @override
  _LoginFormFieldsState createState() => _LoginFormFieldsState();
}

class _LoginFormFieldsState extends State<LoginFormFields> {
  @override
  final _formKey = GlobalKey<FormState>();

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  Widget buttonContent = const ButtonText(text: "Login");

  @override
  void dispose() {
    usernameController.dispose();
    passwordController.dispose();

    super.dispose();
  }

  void startLoading() {
    setState(() {
      buttonContent = const ButtonLoadingScreen();
    });
  }

  void endLoading() {
    setState(() {
      buttonContent = const ButtonText(text: "Login");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FormTextInput(
            labelText: "Username",
            isPassword: false,
            validator: (value) => validateIfEmpty(value),
            controller: usernameController,
          ),
          FormTextInput(
            labelText: "Password",
            isPassword: true,
            validator: (value) => validateIfEmpty(value),
            controller: passwordController,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: ButtonGreenStretch(
              buttonContent,
              () {
                if (_formKey.currentState!.validate()) {
                  _formKey.currentState!.save();
                  SnackBar snackBar = SnackBar(
                    behavior: SnackBarBehavior.floating,
                    content: Text(
                      "Passwords and username don't match.",
                      style: GoogleFonts.rubik(
                        color: const Color.fromRGBO(253, 248, 238, 1),
                        fontSize: 14.0,
                      ),
                    ),
                    backgroundColor: const Color.fromRGBO(125, 84, 49, 1),
                  );
                  startLoading();
                  final validLogin = sendLoginInfo(
                      usernameController.text, passwordController.text);
                  validLogin.then((sessionId) {
                    if (sessionId == null) {
                      endLoading();
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    } else {
                      final preferences = SharedPreferences.getInstance();
                      preferences.then((preferences) {
                        preferences.setString("sessionId", sessionId);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (_) => StotesHome()),
                        );
                      });
                    }
                  });
                }
              },
              fontSize: 21.5,
            ),
          ),
        ],
      ),
    );
  }
}

class RotatingText extends StatelessWidget {
  const RotatingText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          "Be ",
          style: GoogleFonts.rubik(
            fontSize: 35.0,
            color: Color.fromRGBO(52, 30, 11, 1),
            fontWeight: FontWeight.w400,
          ),
        ),
        SizedBox(
          height: 100,
          child: AnimatedTextKit(
            animatedTexts: [
              RotateAnimatedText(
                "Organized.",
                textStyle: GoogleFonts.rubik(
                  fontSize: 35.0,
                  color: Color.fromRGBO(125, 84, 49, 1),
                  fontWeight: FontWeight.w400,
                ),
              ),
              RotateAnimatedText(
                "Connected.",
                textStyle: GoogleFonts.rubik(
                  fontSize: 35.0,
                  color: Color.fromRGBO(125, 84, 49, 1),
                  fontWeight: FontWeight.w400,
                ),
              ),
              RotateAnimatedText(
                "Productive.",
                textStyle: GoogleFonts.rubik(
                  fontSize: 35.0,
                  color: Color.fromRGBO(125, 84, 49, 1),
                  fontWeight: FontWeight.w400,
                ),
              ),
              RotateAnimatedText(
                "Stotes.",
                textStyle: const TextStyle(
                  fontFamily: "MADESunflower",
                  fontSize: 40.0,
                  color: Color.fromRGBO(52, 30, 11, 1),
                  fontWeight: FontWeight.w400,
                ),
              ),
            ],
            repeatForever: true,
          ),
        )
      ],
    );
  }
}
