import "package:flutter/material.dart";
import "package:mobileapp/production.dart";
import 'package:mobileapp/screen/account/profile.dart';
import 'package:mobileapp/screen/project/specific_projects.dart';
import 'package:mobileapp/widgets/cards.dart';
import "package:mobileapp/widgets/navigation.dart";
import "package:mobileapp/widgets/smallwidgets.dart";
import "package:mobileapp/widgets/form.dart";
import "package:mobileapp/model/models.dart";
import "package:google_fonts/google_fonts.dart";
import "package:http/http.dart" as http;
import "dart:convert";
import 'package:shared_preferences/shared_preferences.dart';

Future<User> fetchCurrentUser() async {
  final preferences = await SharedPreferences.getInstance();
  String sessionId = preferences.getString("sessionId") ?? "";
  final response = await http.get(
    Uri.parse("${fetchUrl}account/getuser/"),
    headers: {
      "sessionId": sessionId,
    },
  );
  if (response.statusCode == 200) {
    final body = json.decode(response.body);
    final user = User(
      firstName: body["first_name"],
      lastName: body["last_name"],
      username: body["username"],
      id: body["id"],
      email: body["email"],
    );
    return user;
  } else {
    throw Exception("Failed to load current user");
  }
}

class EditProfileScreen extends StatefulWidget {
  const EditProfileScreen({Key? key}) : super(key: key);

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Navbar(),
      backgroundColor: const Color.fromRGBO(255, 250, 242, 1),
      body: SingleChildScrollView(
        child: EditProfileForm(),
      ),
    );
  }
}

class EditProfileForm extends StatefulWidget {
  const EditProfileForm({Key? key}) : super(key: key);

  @override
  _EditProfileFormState createState() => _EditProfileFormState();
}

class _EditProfileFormState extends State<EditProfileForm> {
  final _formKey = GlobalKey<FormState>();
  late Future<User> user;
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();

  @override
  void initState() {
    super.initState();
    user = fetchCurrentUser();
  }

  Future<void> editProfile(
    String firstName,
    String lastName,
  ) async {
    Map<String, dynamic> rawProject = {
      "firstName": firstName,
      "lastName": lastName,
    };

    final preferences = await SharedPreferences.getInstance();
    String sessionId = preferences.getString("sessionId") ?? "";
    final response = await http.post(
      Uri.parse("${fetchUrl}account/getuser/"),
      body: json.encode(rawProject),
      headers: <String, String>{
        "Content-Type": "application/json; charset=UTF-8",
        "sessionId": sessionId,
      },
    );

    if (response.statusCode == 204) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (_) => ProfileScreen(),
        ),
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(
            "An error have occured.",
            style: GoogleFonts.rubik(
              color: const Color.fromRGBO(253, 248, 238, 1),
              fontSize: 14.0,
            ),
          ),
          backgroundColor: const Color.fromRGBO(125, 84, 49, 1),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<User>(
        future: user,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.hasData) {
            User fetcheduser = snapshot.data!;
            firstNameController.text = fetcheduser.firstName ?? '';
            lastNameController.text = fetcheduser.lastName ?? '';
            return Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.all(25.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Heading("Edit Profile", 48.0),
                    FormTextInput(
                      labelText: "First Name",
                      isPassword: false,
                      controller: firstNameController,
                      validator: (value) => validateIfEmpty(value),
                    ),
                    FormTextInput(
                      labelText: "Last Name",
                      isPassword: false,
                      controller: lastNameController,
                      validator: (value) => validateIfEmpty(value),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ButtonBeige(
                            const ButtonText(
                                text: "Back",
                                color: Color.fromRGBO(52, 30, 11, 1)),
                            () => Navigator.pop(context),
                          ),
                          ButtonGreen(
                            const ButtonText(text: "Edit Profile"),
                            () {
                              if (_formKey.currentState!.validate()) {
                                _formKey.currentState!.save();
                                editProfile(
                                  firstNameController.text,
                                  lastNameController.text,
                                );
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          } else if (snapshot.hasError) {
            return ErrorCard();
          }
          return const LoadingIndicator();
        });
  }
}
