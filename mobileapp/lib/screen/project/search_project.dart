import 'dart:async';
import 'dart:convert';

import "package:flutter/material.dart";
import 'package:google_fonts/google_fonts.dart';
import 'package:mobileapp/model/models.dart';
import "package:http/http.dart" as http;

import "package:mobileapp/production.dart";
import 'package:mobileapp/screen/project/join_project.dart';
import 'package:mobileapp/screen/project/specific_projects.dart';
import 'package:mobileapp/widgets/cards.dart';
import 'package:mobileapp/widgets/carousel.dart';
import 'package:mobileapp/widgets/form.dart';
import 'package:mobileapp/widgets/navigation.dart';
import 'package:mobileapp/widgets/search_bar.dart';
import 'package:mobileapp/widgets/smallwidgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

Future<List<Project>> fetchProject(String query, int pageNum) async {
  final preferences = await SharedPreferences.getInstance();
  String sessionId = preferences.getString("sessionId") ?? "";

  final response = await http.get(
    Uri.parse("${fetchUrl}search-projects/"),
    headers: {
      "sessionId": sessionId,
      "query": query,
      "pageNum": pageNum.toString()
    },
  );

  if (response.statusCode == 200) {
    final projectData = json.decode(json.decode(response.body)["projects"]);
    return projectData.map<Project>((json) => Project.fromMap(json)).toList();
  } else {
    throw Exception("Failed to load projects");
  }
}

Future<bool> hasMoreProjects(String query, int pageNum) async {
  final preferences = await SharedPreferences.getInstance();
  String sessionId = preferences.getString("sessionId") ?? "";

  final response = await http.get(
    Uri.parse("${fetchUrl}has-more/"),
    headers: {
      "sessionId": sessionId,
      "query": query,
      "pageNum": pageNum.toString()
    },
  );

  return response.statusCode == 200;
}

class SearchProjects extends StatefulWidget {
  const SearchProjects({Key? key}) : super(key: key);

  @override
  _SearchProjectsState createState() => _SearchProjectsState();
}

class _SearchProjectsState extends State<SearchProjects> {
  late Future<List<Project>> allProjects;
  final TextEditingController _filter = TextEditingController();
  String query = "";

  final _formKey = GlobalKey<FormState>();
  final queryController = TextEditingController();
  Widget searchButton = const ButtonText(text: "Search Project");

  int pageNum = 0;
  bool hasMore = false;
  Widget nextButton = const ButtonText(text: ">");
  Widget previousButton = const ButtonText(text: "<");

  Future<void> searchProjects(String userQuery) async {
    setState(() {
      query = userQuery;
      pageNum = 0;
      allProjects = fetchProject(query, pageNum);
      hasMoreProjects(query, pageNum + 1).then((futureHasMore) {
        setState(() {
          hasMore = futureHasMore;
        });
      });
    });
  }

  @override
  void initState() {
    allProjects = fetchProject(query, pageNum);
    hasMoreProjects(query, pageNum + 1).then((futureHasMore) {
      setState(() {
        hasMore = futureHasMore;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Navbar(),
      drawer: NavDrawer(),
      body: SingleChildScrollView(
          child: Column(
        children: [
          Heading("Search Projects", 30.0),
          PaddingContainer(
            RubikText(
              "Let's join some projects!",
              16.0,
              textAlign: TextAlign.justify,
            ),
          ),
          Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.all(25.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    FormTextInput(
                      labelText: "",
                      isPassword: false,
                      controller: queryController,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ButtonBeige(
                            const ButtonText(
                                text: "Back",
                                color: Color.fromRGBO(52, 30, 11, 1)),
                            () => Navigator.pop(context),
                          ),
                          ButtonGreen(
                            searchButton,
                            () {
                              searchProjects(
                                queryController.text,
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          FutureBuilder<List<Project>>(
              future: allProjects,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Column(
                      children: snapshot.data!
                          .map<Widget>(
                            (project) => CarouselCard(
                                name: project.name,
                                date: project.endDate,
                                callback: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            JoinProject(pk: project.id)),
                                  );
                                }),
                          )
                          .toList());
                } else if (snapshot.hasError) {
                  return LoadingIndicator();
                }
                return LoadingIndicator();
              }),
          LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constraints) {
            if (pageNum == 0 && hasMore) {
              return Center(
                child:
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 60.0),
                      child: ButtonGreen(nextButton, () {
                        setState(() {
                          
                          hasMoreProjects(query, pageNum + 2)
                              .then((futureHasMore) {
                            setState(() {
                              pageNum += 1;
                              hasMore = futureHasMore;
                              allProjects = fetchProject(query, pageNum);
                            });
                            
                          });
                        });
                      })),
                ]),
              );
            } else if (pageNum > 0 && !hasMore) {
              return Center(
                child:
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 60.0),
                      child: ButtonGreen(previousButton, () {
                        setState(() {
                          hasMoreProjects(query, pageNum)
                              .then((futureHasMore) {
                            setState(() {
                              pageNum -= 1;
                              hasMore = futureHasMore;
                              allProjects = fetchProject(query, pageNum);
                            });
                          });
                        });
                      })),
                ]),
              );
            } else if (pageNum > 0 && hasMore) {
              return Center(
                child:
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 60.0),
                      child: ButtonGreen(previousButton, () {
                          setState(() {
                          
                          hasMoreProjects(query, pageNum)
                              .then((futureHasMore) {
                            setState(() {
                              pageNum -= 1;
                              hasMore = futureHasMore;
                              allProjects = fetchProject(query, pageNum);
                            });
                          });
                        });
                        
                      })),
                  Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 60.0),
                      child: ButtonGreen(nextButton, () {
                        setState(() {
                          
                          
                          hasMoreProjects(query, pageNum + 2)
                              .then((futureHasMore) {
                            setState(() {
                              pageNum += 1;
                              hasMore = futureHasMore;
                              allProjects = fetchProject(query, pageNum);
                            });
                          });
                        });
                      })),
                ]),
              );
            } else {
              return Row(children: []);
            }
          })
        ],
      )),
      backgroundColor: const Color.fromRGBO(255, 250, 242, 1),
    );
  }
}

class Projects {
  late Stream<List<Project>> projectstream;
  late bool hasMore;

  late bool _isLoading;
  late List<Project> _data;
  late StreamController<List<Project>> _controller;

  Projects() {
    _data = <Project>[];
    _controller = StreamController<List<Project>>.broadcast();
    _isLoading = false;
    projectstream = _controller.stream.map((List<Project> projectsData) {
      return projectsData.map((Project projectsData) {
        return Project.fromMap(projectsData.toMap());
      }).toList();
    });
    hasMore = true;
    refresh();
  }

  Future<void> refresh({String refQuery = '', int refPageNum = 0}) {
    return loadMore(
        clearCachedData: true, query: refQuery, pageNum: refPageNum);
  }

  Future<void> loadMore(
      {bool clearCachedData = false, String query = '', int pageNum = 0}) {
    if (clearCachedData) {
      _data = <Project>[];
      hasMore = true;
    }
    if (_isLoading || !hasMore) {
      return Future.value();
    }
    _isLoading = true;
    return fetchProject(query, pageNum).then((projectsData) {
      _isLoading = false;
      _data.addAll(projectsData);
      hasMore = (_data.length < 30);
      _controller.add(_data);
    });
  }
}
