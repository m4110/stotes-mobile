import "dart:convert";

import "package:flutter/material.dart";
import "package:mobileapp/widgets/navigation.dart";
import 'package:mobileapp/widgets/smallwidgets.dart';
import 'package:mobileapp/widgets/boards.dart';
import 'package:mobileapp/widgets/cards.dart';
import "package:mobileapp/model/models.dart";
import "package:mobileapp/screen/task/add_task.dart";
import "package:mobileapp/screen/meeting/add_meeting.dart";
import "package:mobileapp/production.dart";
import "package:shared_preferences/shared_preferences.dart";

import "package:google_fonts/google_fonts.dart";
import "package:http/http.dart" as http;

Future<Project> fetchCurrentProject(int pk) async {
  final response = await http.get(Uri.parse("${fetchUrl}project/$pk"));
  final currentProject =
      json.decode(json.decode(response.body)["chosenProject"]);

  return Project.fromMap(currentProject[0]);
}

int getRemainingDays(DateTime endDate) {
    return endDate.difference(DateTime.now()).inDays;
  }

Future<List<Task>> fetchAllTasks(int pk) async {
  final response = await http.get(Uri.parse("${fetchUrl}project/$pk"));
  final allTasks = json.decode(json.decode(response.body)["taskLst"]);

  return allTasks.map<Task>((task) => Task.fromMap(task)).toList();
}

Future<List<Meeting>> fetchAllMeetings(int pk) async {
  final response = await http.get(Uri.parse("${fetchUrl}project/$pk"));
  final allMeetings = json.decode(json.decode(response.body)["meetingList"]);

  return allMeetings
      .map<Meeting>((meeting) => Meeting.fromMap(meeting))
      .toList();
}

class SpecificProject extends StatefulWidget {
  final int pk;
  const SpecificProject({Key? key, required this.pk}) : super(key: key);

  @override
  _SpecificProjectState createState() => _SpecificProjectState();
}

class _SpecificProjectState extends State<SpecificProject> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();
  late Future<Project> currentProject;
  late Future<List<Task>> allTasks;
  late Future<List<Meeting>> allMeetings;

  @override
  void initState() {
    super.initState();
    currentProject = fetchCurrentProject(widget.pk);
    allTasks = fetchAllTasks(widget.pk);
    allMeetings = fetchAllMeetings(widget.pk);
  }

  Future<void> refreshData() async {
    setState(() {
      currentProject = fetchCurrentProject(widget.pk);
      allTasks = fetchAllTasks(widget.pk);
      allMeetings = fetchAllMeetings(widget.pk);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Navbar(),
      body: FutureBuilder<Project>(
          future: currentProject,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                snapshot.hasData) {
              Project project = snapshot.data!;
              return RefreshIndicator(
                onRefresh: () => refreshData(),
                child: ListView(
                  children: [
                    Heading(project.name, 30.0),
                    PaddingContainer(
                      RubikText(
                        project.description,
                        16.0,
                        textAlign: TextAlign.justify,
                      ),
                    ),
                    PaddingContainer(
                      Row(
                        children: [
                          const Icon(
                            Icons.alarm,
                            color: Color.fromRGBO(52, 30, 11, 1),
                            size: 19.5,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Text(
                              "Due in: ${getRemainingDays(project.endDate)} days",
                              style: GoogleFonts.rubik(
                                fontSize: 17.5,
                                color: const Color.fromRGBO(52, 30, 11, 1),
                                fontWeight: FontWeight.w600,
                                shadows: <Shadow>[
                                  const Shadow(
                                    offset: Offset(2, 2),
                                    blurRadius: 15.0,
                                    color: Color.fromRGBO(120, 120, 120, 0.15),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const Divider(
                      thickness: 1.0,
                    ),
                    Heading("Tasks", 24.0),
                    Align(
                      child: Padding(
                        child: ButtonBeige(
                            const ButtonText(
                              text: "Add Task",
                              color: Color.fromRGBO(52, 30, 11, 1),
                            ), () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  AddTaskScreen(projectId: project.id),
                            ),
                          );
                        }),
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.05),
                      ),
                      alignment: Alignment.topLeft,
                    ),
                    FutureBuilder<List<Task>>(
                        future: allTasks,
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                                  ConnectionState.done &&
                              snapshot.hasData) {
                            return TaskBoard(
                              children: snapshot.data!,
                            );
                          } else if (snapshot.hasError) {
                            return ErrorCard();
                          }

                          return LoadingIndicator();
                        }),
                    Heading("Meetings", 24.0),
                    Align(
                      child: Padding(
                        child: ButtonBeige(
                            const ButtonText(
                              text: "Add Meeting",
                              color: Color.fromRGBO(52, 30, 11, 1),
                            ), () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  AddMeetingScreen(projectId: project.id),
                            ),
                          );
                        }),
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.05),
                      ),
                      alignment: Alignment.topLeft,
                    ),
                    FutureBuilder<List<Meeting>>(
                        future: allMeetings,
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                                  ConnectionState.done &&
                              snapshot.hasData) {
                            return MeetingBoard(
                              children: snapshot.data!,
                            );
                          } else if (snapshot.hasError) {
                            return ErrorCard();
                          }

                          return LoadingIndicator();
                        }),
                  ],
                ),
              );
            } else if (snapshot.hasError) {
              return Center(
                child: ErrorCard(),
              );
            }
            return const Center(
              child: LoadingIndicator(),
            );
          }),
      drawer: NavDrawer(),
      backgroundColor: const Color.fromRGBO(255, 250, 242, 1),
    );
  }
}
