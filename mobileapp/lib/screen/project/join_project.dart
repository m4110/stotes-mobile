import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mobileapp/model/models.dart';
import "package:http/http.dart" as http;
import 'package:mobileapp/screen/project/specific_projects.dart';
import 'package:mobileapp/widgets/form.dart';
import 'package:mobileapp/widgets/navigation.dart';
import 'package:mobileapp/widgets/smallwidgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import "dart:convert";
import '../../production.dart';

Future<Project> fetchCurrentProject(int pk) async {
  final response = await http.get(Uri.parse("${fetchUrl}project/$pk"));
  final currentProject =
      json.decode(json.decode(response.body)["chosenProject"]);

  return Project.fromMap(currentProject[0]);
}

class JoinProject extends StatefulWidget {
  final int pk;
  const JoinProject({Key? key, required this.pk}) : super(key: key);

  @override
  _JoinProjectState createState() => _JoinProjectState();
}

class _JoinProjectState extends State<JoinProject> {
  late Future<Project> selectedProject;

  @override
  void initState() {
    super.initState();
    selectedProject = fetchCurrentProject(widget.pk);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Navbar(),
      body: SingleChildScrollView(
        child: JoinProjectForm(p_id: widget.pk),
      ),
      backgroundColor: const Color.fromRGBO(255, 250, 242, 1),
    );
  }
}

class JoinProjectForm extends StatefulWidget {
  final int p_id;
  const JoinProjectForm({Key? key, required this.p_id}) : super(key: key);

  @override
  _JoinProjectFormState createState() => _JoinProjectFormState();
}

class _JoinProjectFormState extends State<JoinProjectForm> {
  final _formKey = GlobalKey<FormState>();
  final passcodeController = TextEditingController();

  Widget submitButton = const ButtonText(text: "Join Project");
  Future<void> joinProject(String passcode) async {
    Map<String, dynamic> userInput = {
      "passcode": passcode,
    };

    final preferences = await SharedPreferences.getInstance();
    final sessionId = preferences.getString("sessionId");

    final response = await http.post(
      Uri.parse("${fetchUrl}project/join/${widget.p_id}/"),
      headers: <String, String>{
        "Content-Type": "application/json; charset=UTF-8",
        "sessionId": sessionId!,
      },
      body: json.encode(userInput),
    );

    if (response.statusCode == 200) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (_) => SpecificProject(pk: widget.p_id),
        ),
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(
            "The password is incorrect, please try again.",
            style: GoogleFonts.rubik(
              color: const Color.fromRGBO(253, 248, 238, 1),
              fontSize: 14.0,
            ),
          ),
          backgroundColor: const Color.fromRGBO(125, 84, 49, 1),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.all(25.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Heading("Join Project", 40.0),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: RubikText(
                  "Please enter the project's passcode!",
                  18.0,
                ),
              ),
              FormTextInput(
                labelText: "Passcode",
                isPassword: true,
                controller: passcodeController,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ButtonBeige(
                      const ButtonText(
                          text: "Back", color: Color.fromRGBO(52, 30, 11, 1)),
                      () => Navigator.pop(context),
                    ),
                    ButtonGreen(
                      submitButton,
                      () {
                        joinProject(
                          passcodeController.text,
                        );
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
