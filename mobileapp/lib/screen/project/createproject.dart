import "package:flutter/material.dart";
import "package:mobileapp/production.dart";
import 'package:mobileapp/screen/project/specific_projects.dart';
import 'package:mobileapp/widgets/cards.dart';
import "package:mobileapp/widgets/navigation.dart";
import "package:mobileapp/widgets/smallwidgets.dart";
import "package:mobileapp/widgets/form.dart";
import "package:mobileapp/model/models.dart";
import "package:google_fonts/google_fonts.dart";
import "package:http/http.dart" as http;
import "dart:convert";
import 'package:shared_preferences/shared_preferences.dart';

class CreateProjectsScreen extends StatefulWidget {
  const CreateProjectsScreen({Key? key}) : super(key: key);

  @override
  _CreateProjectsScreenState createState() => _CreateProjectsScreenState();
}

class _CreateProjectsScreenState extends State<CreateProjectsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Navbar(),
      body: SingleChildScrollView(
        child: CreateProjectsForm(),
      ),
      backgroundColor: const Color.fromRGBO(255, 250, 242, 1),
    );
  }
}

class CreateProjectsForm extends StatefulWidget {
  const CreateProjectsForm({Key? key}) : super(key: key);

  @override
  _CreateProjectsFormState createState() => _CreateProjectsFormState();
}

class _CreateProjectsFormState extends State<CreateProjectsForm> {
  final _formKey = GlobalKey<FormState>();
  final nameController = TextEditingController();
  final descriptionController = TextEditingController();
  final dateController = TextEditingController();
  final passcodeController = TextEditingController();
  Widget buttonContent = const ButtonText(text: "Create Project");
  Future<void> createProject(String name, String description, String passcode,
      DateTime enddate) async {
    Map<String, dynamic> rawProject = {
      "name": name,
      "description": description,
      "passcode": passcode,
      "endDate": enddate.toString(),
    };

    final preferences = await SharedPreferences.getInstance();
    final sessionId = preferences.getString("sessionId");

    final response = await http.post(
      Uri.parse("${fetchUrl}project/create/"),
      headers: <String, String>{
        "Content-Type": "application/json; charset=UTF-8",
        "sessionId": sessionId!,
      },
      body: json.encode(rawProject),
    );

    if (response.statusCode == 200) {
      int id = json.decode(response.body)["id"];
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (_) => SpecificProject(pk: id),
        ),
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          content: Text(
            "An error have occured.",
            style: GoogleFonts.rubik(
              color: const Color.fromRGBO(253, 248, 238, 1),
              fontSize: 14.0,
            ),
          ),
          backgroundColor: const Color.fromRGBO(125, 84, 49, 1),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.all(25.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Heading("Create Project", 40.0),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: RubikText(
                  "Hello! Let's start creating your project!",
                  18.0,
                ),
              ),
              FormTextInput(
                labelText: "Name",
                isPassword: false,
                controller: nameController,
                validator: (value) => validateIfEmpty(value),
              ),
              FormTextArea(
                name: "Description",
                controller: descriptionController,
              ),
              FormTextInput(
                labelText: "Passcode",
                isPassword: true,
                controller: passcodeController,
              ),
              RubikText(
                "End Date: ",
                18.0,
              ),
              DatePickerButton(
                controller: dateController,
                initDate: DateTime.now(),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ButtonBeige(
                      const ButtonText(
                          text: "Back", color: Color.fromRGBO(52, 30, 11, 1)),
                      () => Navigator.pop(context),
                    ),
                    ButtonGreen(
                      buttonContent,
                      () {
                        createProject(
                          nameController.text,
                          descriptionController.text,
                          passcodeController.text,
                          DateTime.parse(dateController.text),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
