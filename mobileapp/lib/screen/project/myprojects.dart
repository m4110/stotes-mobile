import 'package:flutter/material.dart';
import 'package:mobileapp/model/models.dart';
import "package:http/http.dart" as http;
import 'package:mobileapp/screen/project/createproject.dart';
import 'package:mobileapp/screen/project/specific_projects.dart';
import 'package:mobileapp/widgets/cards.dart';
import 'package:mobileapp/widgets/carousel.dart';
import 'package:mobileapp/widgets/form.dart';
import 'package:mobileapp/widgets/navigation.dart';
import 'package:mobileapp/widgets/smallwidgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import "dart:convert";
import '../../production.dart';

Future<List<Project>> fetchProject(String query, int pageNum) async {
  final preferences = await SharedPreferences.getInstance();
  String sessionId = preferences.getString("sessionId") ?? "";

  final response = await http.get(
    Uri.parse("${fetchUrl}my-projects/"),
    headers: {
      "sessionId": sessionId,
      "query": query,
      "pageNum": pageNum.toString()
    },
  );

  if (response.statusCode == 200) {
    final projectData = json.decode(json.decode(response.body)["projects"]);
    return projectData.map<Project>((json) => Project.fromMap(json)).toList();
  } else {
    throw Exception("Failed to load projects");
  }
}

Future<bool> hasMoreProjects(String query, int pageNum) async {
  final preferences = await SharedPreferences.getInstance();
  String sessionId = preferences.getString("sessionId") ?? "";

  final response = await http.get(
    Uri.parse("${fetchUrl}my-has-more/"),
    headers: {
      "sessionId": sessionId,
      "query": query,
      "pageNum": pageNum.toString()
    },
  );

  return response.statusCode == 200;
}

class MyProjects extends StatefulWidget {
  const MyProjects({Key? key}) : super(key: key);

  @override
  _MyProjectsState createState() => _MyProjectsState();
}

class _MyProjectsState extends State<MyProjects> {
  late Future<List<Project>> futureProjects;
  Widget createProjectButton = const ButtonText(text: "Create Project");

  final TextEditingController _filter = TextEditingController();
  String query = "";

  final _formKey = GlobalKey<FormState>();
  final queryController = TextEditingController();
  Widget searchButton = const ButtonText(text: "Search Project");

  int pageNum = 0;
  bool hasMore = false;
  Widget nextButton = const ButtonText(text: ">");
  Widget previousButton = const ButtonText(text: "<");

  Future<void> searchProjects(String userQuery) async {
    setState(() {
      query = userQuery;
      pageNum = 0;
      futureProjects = fetchProject(query, pageNum);
      hasMoreProjects(query, pageNum + 1).then((futureHasMore) {
        setState(() {
          hasMore = futureHasMore;
        });
      });
    });
  }

  @override
  void initState() {
    futureProjects = fetchProject(query, pageNum);
    hasMoreProjects(query, pageNum + 1).then((futureHasMore) {
      setState(() {
        hasMore = futureHasMore;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Navbar(),
      drawer: NavDrawer(),
      body: Container(
          child: ListView(
        children: [
          Heading("My Projects", 30.0),
          PaddingContainer(
            RubikText(
              "Let's take a look at some of your projects!",
              16.0,
              textAlign: TextAlign.justify,
            ),
          ),
          Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 60.0),
              child: ButtonGreen(createProjectButton, () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CreateProjectsScreen()),
                );
              })),
          Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.all(25.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    FormTextInput(
                      labelText: "",
                      isPassword: false,
                      controller: queryController,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ButtonBeige(
                            const ButtonText(
                                text: "Back",
                                color: Color.fromRGBO(52, 30, 11, 1)),
                            () => Navigator.pop(context),
                          ),
                          ButtonGreen(
                            searchButton,
                            () {
                              searchProjects(
                                queryController.text,
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          FutureBuilder<List<Project>>(
              future: futureProjects,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Column(
                      children: snapshot.data!
                          .map<Widget>(
                            (project) => CarouselCard(
                                name: project.name,
                                date: project.endDate,
                                callback: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            SpecificProject(pk: project.id)),
                                  );
                                }),
                          )
                          .toList());
                } else if (snapshot.hasError) {
                  return LoadingIndicator();
                }
                return LoadingIndicator();
              }),
          LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constraints) {
            if (pageNum == 0 && hasMore) {
              return Center(
                child:
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 60.0),
                      child: ButtonGreen(nextButton, () {
                        setState(() {
                          hasMoreProjects(query, pageNum + 2)
                              .then((futureHasMore) {
                            setState(() {
                              pageNum += 1;
                              hasMore = futureHasMore;
                              futureProjects = fetchProject(query, pageNum);
                            });
                          });
                        });
                      })),
                ]),
              );
            } else if (pageNum > 0 && !hasMore) {
              return Center(
                child:
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 60.0),
                      child: ButtonGreen(previousButton, () {
                        setState(() {
                          hasMoreProjects(query, pageNum).then((futureHasMore) {
                            setState(() {
                              pageNum -= 1;
                              hasMore = futureHasMore;
                              futureProjects = fetchProject(query, pageNum);
                            });
                          });
                        });
                      })),
                ]),
              );
            } else if (pageNum > 0 && hasMore) {
              return Center(
                child:
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 60.0),
                      child: ButtonGreen(previousButton, () {
                        setState(() {
                          hasMoreProjects(query, pageNum).then((futureHasMore) {
                            setState(() {
                              pageNum -= 1;
                              hasMore = futureHasMore;
                              futureProjects = fetchProject(query, pageNum);
                            });
                          });
                        });
                      })),
                  Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 8.0, horizontal: 60.0),
                      child: ButtonGreen(nextButton, () {
                        setState(() {
                          hasMoreProjects(query, pageNum + 2)
                              .then((futureHasMore) {
                            setState(() {
                              pageNum += 1;
                              hasMore = futureHasMore;
                              futureProjects = fetchProject(query, pageNum);
                            });
                          });
                        });
                      })),
                ]),
              );
            } else {
              return Row(children: []);
            }
          })
        ],
      )),
      backgroundColor: const Color.fromRGBO(255, 250, 242, 1),
    );
  }
}
