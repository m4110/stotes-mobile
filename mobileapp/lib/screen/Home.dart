// ignore_for_file: file_names
import "dart:convert";
import "dart:async";
import "package:flutter/material.dart";
import "package:http/http.dart" as http;
import 'package:mobileapp/screen/task/task.dart';
import "package:shared_preferences/shared_preferences.dart";

import "package:mobileapp/widgets/carousel.dart";
import "package:mobileapp/widgets/navigation.dart";
import "package:mobileapp/widgets/smallwidgets.dart";
import "package:mobileapp/widgets/cards.dart";
import "package:mobileapp/model/models.dart";
import "package:mobileapp/production.dart";

import "package:mobileapp/screen/project/specific_projects.dart";

Future<List<dynamic>> fetchNewsfeed() async {
  final String url =
      "https://free-news.p.rapidapi.com/v1/search?q=team%20work&lang=en";

  var result = await http.get(Uri.parse(url), headers: {
    "x-rapidapi-host": "free-news.p.rapidapi.com",
    "x-rapidapi-key": "76c6169c64msh23684ee736c9bc0p18d4b1jsna587ff102f57"
  });

  if (result.statusCode == 200) {
    return json.decode(result.body)["articles"];
  } else {
    throw Exception("Failed loading newsfeed :(");
  }
}

Future<List<Project>> fetchProject() async {
  final preferences = await SharedPreferences.getInstance();
  String sessionId = preferences.getString("sessionId") ?? "";

  final response = await http.get(
    Uri.parse("${fetchUrl}home/"),
    headers: {
      "sessionId": sessionId,
    },
  );

  if (response.statusCode == 200) {
    final projectData = json.decode(json.decode(response.body)["projects"]);
    return projectData.map<Project>((json) => Project.fromMap(json)).toList();
  } else {
    throw Exception("Failed to load projects");
  }
}

Future<List<Task>> fetchTask() async {
  final preferences = await SharedPreferences.getInstance();
  String sessionId = preferences.getString("sessionId") ?? "";

  final response = await http.get(
    Uri.parse("${fetchUrl}home/"),
    headers: {
      "sessionId": sessionId,
    },
  );

  if (response.statusCode == 200) {
    final latestTask = json.decode(json.decode(response.body)["tasks"]);
    return latestTask.map<Task>((task) => Task.fromMap(task)).toList();
  } else {
    throw Exception("Failed to load tasks");
  }
}

Future<String> getUsername() async {
  final preferences = await SharedPreferences.getInstance();
  String sessionId = preferences.getString("sessionId") ?? "";
  final response = await http.get(
    Uri.parse("${fetchUrl}account/getuser/"),
    headers: {
      "sessionId": sessionId,
    },
  );
  if (response.statusCode == 200) {
    final username = json.decode(response.body)["username"];
    return username;
  } else {
    throw Exception("Failed to load current user");
  }
}

class StotesHome extends StatefulWidget {
  const StotesHome({Key? key}) : super(key: key);

  @override
  _StotesHomeState createState() => _StotesHomeState();
}

class _StotesHomeState extends State<StotesHome> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  late Future<List<Project>> futureProjects;
  late Future<List<Task>> latestTasks;
  late Future<String> username;

  @override
  void initState() {
    super.initState();
    futureProjects = fetchProject();
    latestTasks = fetchTask();
    username = getUsername();
  }

  Future<void> refreshAction() async {
    _refreshIndicatorKey.currentState!.show();
    setState(() {
      futureProjects = fetchProject();
      latestTasks = fetchTask();
      username = getUsername();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Navbar(),
      drawer: NavDrawer(),
      body: Container(
        child: RefreshIndicator(
          key: _refreshIndicatorKey,
          onRefresh: () => refreshAction(),
          child: ListView(
            children: <Widget>[
              FutureBuilder(
                future: username,
                builder: (context, snapshot) {
                  if (snapshot.hasData &&
                      snapshot.connectionState == ConnectionState.done) {
                    return Heading("Hello, ${snapshot.data}!", 37.5);
                  } else if (snapshot.hasError) {
                    return ErrorCard();
                  }
                  return LoadingIndicator();
                },
              ),
              Heading("Recent Projects", 30.0),
              FutureBuilder<List<Project>>(
                  future: futureProjects,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return MainCarousel(
                          children: snapshot.data!
                              .map<Widget>(
                                (project) => CarouselCard(
                                    name: project.name,
                                    date: project.endDate,
                                    callback: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                SpecificProject(
                                                    pk: project.id)),
                                      );
                                    }),
                              )
                              .toList());
                    } else if (snapshot.hasError) {
                      return ErrorCard();
                    }
                    return const LoadingIndicator();
                  }),
              Heading("Recent Tasks", 30.0),
              FutureBuilder<List<Task>>(
                future: latestTasks,
                builder: (build, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.hasData) {
                    return MainCarousel(
                        children: snapshot.data!
                            .map<Widget>((task) => CarouselCard(
                                name: task.name,
                                date: task.dueDate,
                                type: task.type,
                                callback: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          TaskScreen(pk: task.id),
                                    ),
                                  );
                                }))
                            .toList());
                  } else if (snapshot.hasError) {
                    return ErrorCard();
                  }
                  return const LoadingIndicator();
                },
              ),
              Heading("Newsfeed", 35.0),
              NewsfeedView(),
            ],
          ),
        ),
      ),
      backgroundColor: const Color.fromRGBO(255, 250, 242, 1),
    );
  }
}

class NewsfeedView extends StatefulWidget {
  @override
  State<NewsfeedView> createState() => _NewsfeedViewState();
}

class _NewsfeedViewState extends State<NewsfeedView> {
  late Future<List<dynamic>> newsfeed;

  @override
  void initState() {
    super.initState();
    newsfeed = fetchNewsfeed();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<dynamic>>(
        future: newsfeed,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return Column(
                children: snapshot.data
                    .sublist(1, 4)
                    .map<Widget>((news) => NewsfeedCard(
                        news["title"], news["summary"], news["link"]))
                    .toList());
          } else if (snapshot.hasError) {
            return ErrorCard();
          }
          return const LoadingIndicator();
        });
  }
}
