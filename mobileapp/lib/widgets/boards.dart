import "package:flutter/material.dart";
import "package:flutter_slidable/flutter_slidable.dart";
import 'package:google_fonts/google_fonts.dart';

import "package:mobileapp/model/models.dart";
import "package:mobileapp/production.dart";
import 'package:mobileapp/screen/task/task.dart';
import 'package:mobileapp/screen/task/edit_task.dart';
import 'package:mobileapp/screen/meeting/meeting.dart';
import 'package:mobileapp/screen/meeting/edit_meeting.dart';
import 'package:mobileapp/main.dart';

import 'package:mobileapp/widgets/smallwidgets.dart';
import "package:http/http.dart" as http;

import "dart:async";

Future<bool> deleteTask(int taskId) async {
  final response = await http.delete(
    Uri.parse("${fetchUrl}task/delete/$taskId/"),
  );

  if (response.statusCode == 200) {
    return true;
  }

  return false;
}

Future<bool> deleteMeeting(int meetingId) async {
  final response = await http.delete(
    Uri.parse("${fetchUrl}meeting/delete/$meetingId/"),
  );

  if (response.statusCode == 200) {
    return true;
  }

  return false;
}

class TaskBoard extends StatefulWidget {
  final List<Task> children;

  TaskBoard({Key? key, required this.children}) : super(key: key);

  @override
  _TaskBoardState createState() => _TaskBoardState();
}

class _TaskBoardState extends State<TaskBoard> {
  final listKey = GlobalKey<AnimatedListState>();

  void removeChild(int index) {
    final removedChild = widget.children[index];

    widget.children.remove(removedChild);
    listKey.currentState!.removeItem(
      index,
      (context, animation) {
        int remainingDays = removedChild.getRemainingDays();
        return SizeTransition(
          child: BoardNote(
            id: removedChild.id,
            title: removedChild.name,
            type: removedChild.type,
            subtitle:
                "Due in: $remainingDays ${remainingDays == 1 ? "day" : "days"} ",
            onDelete: () {},
          ),
          sizeFactor: animation,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20),
      child: AnimatedList(
          key: listKey,
          shrinkWrap: true,
          initialItemCount: widget.children.length,
          itemBuilder: (context, index, animation) {
            final data = widget.children[index];
            int remainingDays = data.getRemainingDays();
            return SizeTransition(
              child: BoardNote(
                id: data.id,
                title: data.name,
                type: data.type,
                subtitle:
                    "Due in: $remainingDays ${remainingDays == 1 ? "day" : "days"} ",
                onDelete: () {
                  showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (context) => AlertDialog(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      backgroundColor: Colors.orange[50],
                      titlePadding: const EdgeInsets.all(0),
                      title: Container(
                        decoration: const BoxDecoration(
                          color: Color.fromRGBO(52, 30, 11, 1),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10.0),
                            topRight: Radius.circular(10.0),
                          ),
                        ),
                        child: const Text(
                          "Delete project?",
                          style: TextStyle(
                            fontFamily: "MADESunflower",
                            fontSize: 28.0,
                            color: Colors.white,
                          ),
                        ),
                        padding: const EdgeInsets.symmetric(
                          horizontal: 24.0,
                          vertical: 12.0,
                        ),
                        // color: const Color.fromRGBO(52, 30, 11, 1),
                      ),
                      content: RichText(
                          text: TextSpan(
                        text: "Do you want to delete ",
                        style: GoogleFonts.rubik(
                          fontSize: 16,
                          color: Colors.black,
                        ),
                        children: [
                          TextSpan(
                            text: data.name,
                            style: GoogleFonts.rubik(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          const TextSpan(
                            text: " forever?",
                          ),
                        ],
                      )),
                      actions: [
                        ButtonBeige(
                          const ButtonText(
                            text: "Not this time.",
                            fontSize: 18.0,
                            color: Color.fromRGBO(52, 30, 11, 1),
                          ),
                          () => Navigator.pop(context),
                        ),
                        ButtonRed(
                          child: const ButtonText(
                            text: "Yep, delete.",
                            fontSize: 18,
                          ),
                          callback: () {
                            final deletion = deleteTask(data.id);
                            deletion.then((status) {
                              Navigator.pop(context);
                              Timer(Duration(milliseconds: 250),
                                  () => removeChild(index));
                            });
                          },
                        ),
                      ],
                    ),
                  );
                  // deleteTask(data.id);
                },
              ),
              sizeFactor: animation,
            );
          }),
      width: MediaQuery.of(context).size.width * 0.9,
    );
  }
}

class BoardNote extends StatelessWidget {
  final int id;
  final String title;
  final String type;
  final String subtitle;
  final void Function()? onDelete;

  const BoardNote({
    Key? key,
    required this.id,
    required this.title,
    required this.type,
    required this.subtitle,
    required this.onDelete,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    IconData usedIcon;
    switch (type) {
      case "Presentation":
        usedIcon = Icons.present_to_all;
        break;
      case "Report":
        usedIcon = Icons.book;
        break;
      case "Design":
        usedIcon = Icons.brush;
        break;
      case "Social Media":
        usedIcon = Icons.smartphone;
        break;
      case "Assignment":
        usedIcon = Icons.group_work;
        break;
      case "Paper":
        usedIcon = Icons.auto_stories_outlined;
        break;
      case "Meeting":
        usedIcon = Icons.calendar_today;
        break;
      default:
        usedIcon = Icons.remove_circle_outline;
    }
    return Slidable(
      actionPane: SlidableScrollActionPane(),
      actions: [
        Container(
          child: IconSlideAction(
            caption: "Edit",
            color: Color.fromRGBO(102, 108, 78, 1),
            icon: Icons.create,
            onTap: () => Navigator.push(context,
                MaterialPageRoute(builder: (_) => EditTaskScreen(id: id))),
          ),
          margin: const EdgeInsets.symmetric(vertical: 2.00),
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
              left:
                  BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
              bottom:
                  BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
            ),
            boxShadow: [
              BoxShadow(
                  blurRadius: 4,
                  spreadRadius: 3,
                  color: Color.fromRGBO(138, 138, 138, 0.1)),
            ],
          ),
        ),
        Container(
          child: IconSlideAction(
            caption: "Delete",
            color: Color.fromRGBO(163, 82, 73, 1),
            icon: Icons.delete,
            onTap: onDelete,
          ),
          margin: EdgeInsets.symmetric(vertical: 2.00),
          decoration: const BoxDecoration(
            border: Border.symmetric(
              horizontal:
                  BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
              vertical:
                  BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
            ),
            boxShadow: [
              BoxShadow(
                  blurRadius: 4,
                  spreadRadius: 3,
                  color: Color.fromRGBO(138, 138, 138, 0.1)),
            ],
          ),
        ),
        Container(
          child: IconSlideAction(
            caption: "More",
            color: Color.fromRGBO(228, 147, 100, 1),
            icon: Icons.more,
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => TaskScreen(pk: id),
              ),
            ),
          ),
          margin: EdgeInsets.symmetric(vertical: 2.00),
          decoration: const BoxDecoration(
            border: Border.symmetric(
              horizontal:
                  BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
            ),
            boxShadow: [
              BoxShadow(
                  blurRadius: 4,
                  spreadRadius: 3,
                  color: Color.fromRGBO(138, 138, 138, 0.1)),
            ],
          ),
        ),
        Container(
          child: IconSlideAction(
            caption: "Close",
            color: Color.fromRGBO(221, 232, 172, 1),
            icon: Icons.close,
            onTap: () {},
          ),
          margin: const EdgeInsets.symmetric(vertical: 2.00),
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
              left:
                  BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
              bottom:
                  BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
            ),
            boxShadow: [
              BoxShadow(
                  blurRadius: 4,
                  spreadRadius: 3,
                  color: Color.fromRGBO(138, 138, 138, 0.1)),
            ],
          ),
        ),
      ],
      child: Container(
        child: ListTile(
          leading: Icon(
            usedIcon,
            size: 40.0,
            color: Color.fromRGBO(52, 30, 11, 1),
          ),
          title: Text(
            title,
            style: const TextStyle(
              fontFamily: "MADESunflower",
              fontSize: 18.0,
              color: Color.fromRGBO(52, 30, 11, 1),
            ),
          ),
          subtitle: Row(
            children: [
              const Icon(
                Icons.alarm,
                color: Color.fromRGBO(52, 30, 11, 1),
                size: 14.5,
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Text(
                  subtitle,
                  style: GoogleFonts.rubik(
                    fontSize: 12.5,
                    color: const Color.fromRGBO(52, 30, 11, 1),
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ],
          ),
          tileColor: Color.fromRGBO(255, 245, 219, 1),
        ),
        decoration: BoxDecoration(
          border: Border.all(
              color: const Color.fromRGBO(82, 49, 21, 1), width: 1.0),
          boxShadow: const [
            BoxShadow(
                blurRadius: 4,
                spreadRadius: 3,
                color: Color.fromRGBO(138, 138, 138, 0.1)),
          ],
        ),
        margin: EdgeInsets.symmetric(vertical: 2.00),
      ),
    );
  }
}

class MeetingBoard extends StatefulWidget {
  final List<Meeting> children;

  MeetingBoard({Key? key, required this.children}) : super(key: key);

  @override
  _MeetingBoardState createState() => _MeetingBoardState();
}

class _MeetingBoardState extends State<MeetingBoard> {
  final listKey = GlobalKey<AnimatedListState>();

  void removeChild(int index) {
    final removedChild = widget.children[index];

    widget.children.remove(removedChild);
    listKey.currentState!.removeItem(
      index,
      (context, animation) {
        int remainingHours = removedChild.getRemainingHours();

        return SizeTransition(
          child: BoardNoteMeeting(
            id: removedChild.id,
            title: removedChild.name,
            link: removedChild.link,
            dateTime: removedChild.dateTime.toString().substring(0, 16),
            subtitle: remainingHours > 0
                ? remainingHours > 24
                    ? "Starts in " +
                        (remainingHours / 24).floor().toString() +
                        ((remainingHours / 24).floor() == 1 ? " day" : " days")
                    : "Starts in $remainingHours hours"
                : "Started " + remainingHours.abs().toString() + " hours ago",
            remainingHours: remainingHours,
            onDelete: () {},
          ),
          sizeFactor: animation,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20),
      child: AnimatedList(
          key: listKey,
          shrinkWrap: true,
          initialItemCount: widget.children.length,
          itemBuilder: (context, index, animation) {
            final data = widget.children[index];
            int remainingHours = data.getRemainingHours();

            return SizeTransition(
              child: BoardNoteMeeting(
                  id: data.id,
                  title: data.name,
                  link: data.link,
                  remainingHours: remainingHours,
                  dateTime: data.dateTime.toString().substring(0, 16),
                  subtitle: remainingHours > 0
                      ? remainingHours > 24
                          ? "Starts in " +
                              (remainingHours / 24).floor().toString() +
                              ((remainingHours / 24).floor() == 1
                                  ? " day"
                                  : " days")
                          : "Starts in $remainingHours hours"
                      : "Started " +
                          remainingHours.abs().toString() +
                          " hours ago",
                  onDelete: () {
                    showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (context) => AlertDialog(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0)),
                                backgroundColor: Colors.orange[50],
                                titlePadding: const EdgeInsets.all(0),
                                title: Container(
                                  decoration: const BoxDecoration(
                                    color: Color.fromRGBO(52, 30, 11, 1),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10.0),
                                      topRight: Radius.circular(10.0),
                                    ),
                                  ),
                                  child: const Text(
                                    "Delete meeting?",
                                    style: TextStyle(
                                      fontFamily: "MADESunflower",
                                      fontSize: 28.0,
                                      color: Colors.white,
                                    ),
                                  ),
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 24.0,
                                    vertical: 12.0,
                                  ),
                                  // color: const Color.fromRGBO(52, 30, 11, 1),
                                ),
                                content: RichText(
                                    text: TextSpan(
                                  text: "Do you want to delete ",
                                  style: GoogleFonts.rubik(
                                    fontSize: 16,
                                    color: Colors.black,
                                  ),
                                  children: [
                                    TextSpan(
                                      text: data.name,
                                      style: GoogleFonts.rubik(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    const TextSpan(
                                      text: " forever?",
                                    ),
                                  ],
                                )),
                                actions: [
                                  ButtonBeige(
                                    const ButtonText(
                                      text: "Not this time.",
                                      fontSize: 18.0,
                                      color: Color.fromRGBO(52, 30, 11, 1),
                                    ),
                                    () => Navigator.pop(context),
                                  ),
                                  ButtonRed(
                                    child: const ButtonText(
                                      text: "Yep, delete.",
                                      fontSize: 18,
                                    ),
                                    callback: () {
                                      final deletion = deleteMeeting(data.id);
                                      deletion.then((status) {
                                        Navigator.pop(context);
                                        Timer(Duration(milliseconds: 250),
                                            () => removeChild(index));
                                      });
                                    },
                                  ),
                                ]));
                  }),
              sizeFactor: animation,
            );
          }),
      width: MediaQuery.of(context).size.width * 0.9,
    );
  }
}

class BoardNoteMeeting extends StatelessWidget {
  final int id;
  final String title;
  final String link;
  final String subtitle;
  final int remainingHours;
  final String dateTime;
  final void Function()? onDelete;

  const BoardNoteMeeting({
    Key? key,
    required this.id,
    required this.title,
    required this.link,
    required this.subtitle,
    required this.onDelete,
    required this.remainingHours,
    required this.dateTime,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Slidable(
      actionPane: SlidableScrollActionPane(),
      actions: [
        Container(
          child: IconSlideAction(
              caption: "Edit",
              color: Color.fromRGBO(102, 108, 78, 1),
              icon: Icons.create,
              onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => EditMeetingScreen(id: id)))),
          margin: const EdgeInsets.symmetric(vertical: 2.00),
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
              left:
                  BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
              bottom:
                  BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
            ),
            boxShadow: [
              BoxShadow(
                  blurRadius: 4,
                  spreadRadius: 3,
                  color: Color.fromRGBO(138, 138, 138, 0.1)),
            ],
          ),
        ),
        Container(
          child: IconSlideAction(
              caption: "Delete",
              color: Color.fromRGBO(163, 82, 73, 1),
              icon: Icons.delete,
              onTap: onDelete),
          margin: EdgeInsets.symmetric(vertical: 2.00),
          decoration: const BoxDecoration(
            border: Border.symmetric(
              horizontal:
                  BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
              vertical:
                  BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
            ),
            boxShadow: [
              BoxShadow(
                  blurRadius: 4,
                  spreadRadius: 3,
                  color: Color.fromRGBO(138, 138, 138, 0.1)),
            ],
          ),
        ),
        Container(
          child: IconSlideAction(
            caption: "More",
            color: Color.fromRGBO(228, 147, 100, 1),
            icon: Icons.more,
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => MeetingScreen(pk: id),
              ),
            ),
          ),
          margin: EdgeInsets.symmetric(vertical: 2.00),
          decoration: const BoxDecoration(
            border: Border.symmetric(
              horizontal:
                  BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
            ),
            boxShadow: [
              BoxShadow(
                  blurRadius: 4,
                  spreadRadius: 3,
                  color: Color.fromRGBO(138, 138, 138, 0.1)),
            ],
          ),
        ),
        Container(
          child: IconSlideAction(
            caption: "Close",
            color: Color.fromRGBO(221, 232, 172, 1),
            icon: Icons.close,
            onTap: () {},
          ),
          margin: const EdgeInsets.symmetric(vertical: 2.00),
          decoration: const BoxDecoration(
            border: Border(
              top: BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
              left:
                  BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
              bottom:
                  BorderSide(color: Color.fromRGBO(82, 49, 21, 1), width: 1.0),
            ),
            boxShadow: [
              BoxShadow(
                  blurRadius: 4,
                  spreadRadius: 3,
                  color: Color.fromRGBO(138, 138, 138, 0.1)),
            ],
          ),
        ),
      ],
      child: Container(
        child: ListTile(
          leading: Icon(
            remainingHours > 0 ? Icons.people_outline : Icons.people_rounded,
            size: 40.0,
            color: Color.fromRGBO(52, 30, 11, 1),
          ),
          title: Text(
            title,
            style: const TextStyle(
              fontFamily: "MADESunflower",
              fontSize: 18.0,
              color: Color.fromRGBO(52, 30, 11, 1),
            ),
          ),
          subtitle: Column(
            children: [
              Row(
                children: [
                  const Icon(
                    Icons.calendar_today,
                    color: Color.fromRGBO(52, 30, 11, 1),
                    size: 14.5,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Text(
                      dateTime,
                      style: GoogleFonts.rubik(
                        fontSize: 12.5,
                        color: const Color.fromRGBO(52, 30, 11, 1),
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  const Icon(
                    Icons.alarm,
                    color: Color.fromRGBO(52, 30, 11, 1),
                    size: 14.5,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Text(
                      subtitle,
                      style: GoogleFonts.rubik(
                        fontSize: 12.5,
                        color: const Color.fromRGBO(52, 30, 11, 1),
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
          tileColor: Color.fromRGBO(255, 245, 219, 1),
        ),
        decoration: BoxDecoration(
          border: Border.all(
              color: const Color.fromRGBO(82, 49, 21, 1), width: 1.0),
          boxShadow: const [
            BoxShadow(
                blurRadius: 4,
                spreadRadius: 3,
                color: Color.fromRGBO(138, 138, 138, 0.1)),
          ],
        ),
        margin: EdgeInsets.symmetric(vertical: 2.00),
      ),
    );
  }
}
