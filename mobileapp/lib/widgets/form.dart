import "package:flutter/material.dart";
import 'package:google_fonts/google_fonts.dart';
import 'package:mobileapp/widgets/smallwidgets.dart';
import "package:intl/intl.dart";
import "package:multi_select_flutter/multi_select_flutter.dart";
import "package:mobileapp/model/models.dart";

String? validateIfEmpty(String? text) {
  if (text!.isEmpty) {
    return "The field cannot be empty";
  }
  return null;
}

String? validateEmail(String? text) {
  RegExp regex = RegExp(r"^\w+([\.]\w+)*@\w+([\.]\w+)*(\.\w{2,3})$");

  if (text!.isEmpty) {
    return "The field cannot be empty";
  } else if (!regex.hasMatch(text)) {
    return "The email is not valid.";
  }
  return null;
}

String? validatePassword(String? text) {
  RegExp regexCapital = RegExp(r"[A-Z]");
  RegExp regexLowercase = RegExp(r"[a-z]");
  RegExp regexNumber = RegExp(r"[0-9]");
  RegExp regexSpecial = RegExp(r"[$&+,:;=?!@#]");

  if (text!.isEmpty) {
    return "The field cannot be empty";
  } else if (text.length < 5) {
    return "The length must be at least 5 characters.";
  } else if (!regexLowercase.hasMatch(text)) {
    return "It must contain lowercase letters";
  } else if (!regexCapital.hasMatch(text)) {
    return "It must contain uppercase letters.";
  } else if (!regexNumber.hasMatch(text)) {
    return "It must contain digits.";
  } else if (!regexSpecial.hasMatch(text)) {
    return "It must contain \$&+,;=?@#.";
  }
  return null;
}

String? validateDate(String? text) {
  try {
    DateTime.parse(text!);
    return null;
  } catch (e) {
    return "Not a valid date value.";
  }
}

String? validateUrl(String? text) {
  Uri url = Uri.parse(text!);

  if (url.host == "") {
    return 'Please enter a valid URL';
  }

  // if (Uri.parse("http://$text").host == '' ||
  //     Uri.parse("http://www.$text").host == '') {
  //   return 'Please enter a valid URL';
  // }
  return null;
}

class FormTextInput extends StatefulWidget {
  final String labelText;
  final bool isPassword;
  final String? Function(String?)? validator;
  final TextEditingController controller;
  final String? Function(String?)? onSaved;

  const FormTextInput({
    Key? key,
    required this.labelText,
    required this.isPassword,
    this.validator,
    this.onSaved,
    required this.controller,
  }) : super(key: key);

  @override
  State<FormTextInput> createState() => _FormTextInputState();
}

class _FormTextInputState extends State<FormTextInput> {
  late bool isHidden;

  void showPassword() {
    setState(() {
      if (widget.isPassword) {
        isHidden = !isHidden;
      }
    });
  }

  @override
  void initState() {
    super.initState();
    isHidden = widget.isPassword ? true : false;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: TextFormField(
        style: GoogleFonts.rubik(
          color: const Color.fromRGBO(52, 30, 11, 1),
          fontSize: 18,
          fontWeight: FontWeight.normal,
        ),
        obscureText: isHidden,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: const BorderSide(
              color: Color.fromRGBO(52, 30, 11, 1),
              width: 2.0,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: const BorderSide(
              color: Color.fromRGBO(52, 30, 11, 1),
              width: 2.0,
            ),
          ),
          labelText: widget.labelText,
          contentPadding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
          suffixIcon: widget.isPassword
              ? (IconButton(
                  icon: isHidden
                      ? Icon(Icons.visibility)
                      : Icon(Icons.visibility_off),
                  onPressed: () => showPassword(),
                ))
              : null,
        ),
        validator: widget.validator,
        controller: widget.controller,
        onSaved: widget.onSaved,
      ),
    );
  }
}

class FormTextArea extends StatefulWidget {
  final String name;
  final TextEditingController controller;

  const FormTextArea({Key? key, required this.name, required this.controller})
      : super(key: key);

  @override
  _FormTextAreaState createState() => _FormTextAreaState();
}

class _FormTextAreaState extends State<FormTextArea> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: TextFormField(
        expands: false,
        style: GoogleFonts.rubik(
          color: const Color.fromRGBO(52, 30, 11, 1),
          fontSize: 18,
          fontWeight: FontWeight.normal,
        ),
        maxLines: 8,
        controller: widget.controller,
        validator: (value) => validateIfEmpty(value),
        decoration: InputDecoration(
          alignLabelWithHint: true,
          labelText: widget.name,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: const BorderSide(
              color: Color.fromRGBO(52, 30, 11, 1),
              width: 2.0,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: const BorderSide(
              color: Color.fromRGBO(52, 30, 11, 1),
              width: 2.0,
            ),
          ),
        ),
      ),
    );
  }
}

class DropdownMenu extends StatefulWidget {
  final List<String> dropdowns;
  String initValue;
  DropdownMenu({Key? key, required this.dropdowns, required this.initValue})
      : super(key: key);

  String value = "";

  @override
  _DropdownMenuState createState() => _DropdownMenuState();
}

class _DropdownMenuState extends State<DropdownMenu> {
  void changeValue(String? newValue) {
    setState(() {
      widget.value = newValue!;
    });
  }

  @override
  void initState() {
    super.initState();
    widget.value = widget.initValue;
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: const Color.fromRGBO(52, 30, 11, 1),
      ),
      child: Container(
        child: DropdownButton<String>(
          value: widget.value,
          icon: const Icon(
            Icons.keyboard_arrow_down,
            color: Colors.white,
          ),
          elevation: 10,
          items: widget.dropdowns
              .map<DropdownMenuItem<String>>(
                (itemValue) => DropdownMenuItem<String>(
                  value: itemValue,
                  child: Text(
                    itemValue,
                    style: GoogleFonts.rubik(
                      fontSize: 16.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              )
              .toList(),
          onChanged: (value) => changeValue(value),
          underline: SizedBox(),
        ),
        decoration: BoxDecoration(
          border: Border.all(
            color: Color.fromRGBO(52, 30, 11, 1),
            width: 2.0,
          ),
          color: const Color.fromRGBO(52, 30, 11, 1),
          borderRadius: BorderRadius.circular(10.0),
        ),
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        margin: EdgeInsets.symmetric(vertical: 10.0),
      ),
    );
  }
}

class DatePickerButton extends StatefulWidget {
  final TextEditingController controller;
  final DateTime initDate;

  const DatePickerButton({
    Key? key,
    required this.controller,
    required this.initDate,
  }) : super(key: key);

  @override
  _DatePickerButtonState createState() => _DatePickerButtonState();
}

class _DatePickerButtonState extends State<DatePickerButton> {
  late DateTime selectedDate;

  @override
  void initState() {
    super.initState();
    selectedDate = widget.initDate;
    widget.controller.text = DateFormat("yyyy-MM-dd").format(selectedDate);
  }

  Future<void> pickDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2045),
    );

    if (picked != null) {
      setState(() {
        selectedDate = picked;
        widget.controller.clear();
        widget.controller.text = DateFormat("yyyy-MM-dd").format(selectedDate);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: TextFormField(
        style: GoogleFonts.rubik(
          color: const Color.fromRGBO(52, 30, 11, 1),
          fontSize: 18,
          fontWeight: FontWeight.normal,
        ),
        validator: (value) => validateDate(value),
        controller: widget.controller,
        decoration: InputDecoration(
          hintText: "Select Date",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: const BorderSide(
              color: Color.fromRGBO(52, 30, 11, 1),
              width: 2.0,
            ),
          ),
          suffixIcon: IconButton(
            icon: const Icon(Icons.calendar_today),
            color: const Color.fromRGBO(128, 75, 23, 1),
            onPressed: () => pickDate(context),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: const BorderSide(
              color: Color.fromRGBO(52, 30, 11, 1),
              width: 2.0,
            ),
          ),
        ),
      ),
    );
  }
}

class UserMultipleSelect extends StatefulWidget {
  final List<User?> listOfUsers;
  final void Function(List<int?>?)? onTap;
  final Text heading;
  List<int?> userValues;

  UserMultipleSelect(
      {Key? key,
      required this.listOfUsers,
      required this.heading,
      required this.onTap,
      this.userValues = const []})
      : super(key: key);

  @override
  _UserMultipleSelectState createState() => _UserMultipleSelectState();
}

class _UserMultipleSelectState extends State<UserMultipleSelect> {
  @override
  Widget build(BuildContext context) {
    return MultiSelectChipField<int?>(
      initialValue: widget.userValues,
      items: widget.listOfUsers
          .map<MultiSelectItem<int?>>(
              (user) => MultiSelectItem(user!.id, user.username))
          .toList(),
      onTap: widget.onTap,
      icon: Icon(Icons.close),
      title: widget.heading,
      scroll: false,
      headerColor: Color.fromRGBO(52, 30, 11, 1),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        border: Border.all(
          color: const Color.fromRGBO(52, 30, 11, 1),
          width: 2.0,
        ),
      ),
      textStyle: GoogleFonts.rubik(
        fontSize: 13.0,
      ),
      validator: ((users) {
        if (users == null || users.isEmpty) {
          return "Please select at least a participant.";
        }
        return null;
      }),
    );
  }
}
