import "package:flutter/material.dart";
import 'package:intl/intl.dart';
import 'package:mobileapp/widgets/smallwidgets.dart';
import "package:url_launcher/url_launcher.dart";
import "package:google_fonts/google_fonts.dart";

class NewsfeedCard extends StatelessWidget {
  final String title;
  final String snippet;
  final String link;

  NewsfeedCard(this.title, this.snippet, this.link);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
          child: Column(
            children: [
              Padding(
                  child: Align(
                      child: Text(title,
                          style: GoogleFonts.rubik(fontWeight: FontWeight.w800),
                          textAlign: TextAlign.left),
                      alignment: Alignment.topLeft),
                  padding: EdgeInsets.symmetric(vertical: 7.0)),
              Padding(
                  child: Text(snippet),
                  padding: EdgeInsets.symmetric(vertical: 4.0)),
              TextButton(
                child: Text("Read More"),
                onPressed: () => launch(link),
              ),
            ],
          ),
          padding: EdgeInsets.all(20.0)),
      decoration: BoxDecoration(
          color: Color.fromRGBO(253, 248, 238, 1),
          border: Border.all(color: Color.fromRGBO(82, 49, 21, 1), width: 2.0),
          boxShadow: const [
            BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.25),
                offset: Offset(0, 4),
                blurRadius: 4)
          ]),
      width: double.infinity,
      margin: const EdgeInsets.all(10.0),
    );
  }
}

class ErrorCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(30.0),
      padding: EdgeInsets.all(20.0),
      decoration: BoxDecoration(
        color: Colors.orange[50],
        borderRadius: BorderRadius.circular(20.0),
        border: Border.all(
          color: const Color.fromRGBO(52, 30, 11, 1),
          width: 2.0,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Image.asset(
            "assets/images/sadstote.png",
            height: 120,
            width: 120,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Heading("Oops...", 20.0),
              SizedBox(
                  width: MediaQuery.of(context).size.width * 0.4,
                  child: RubikText("Something went wrong...", 16.0)),
            ],
          ),
        ],
      ),
    );
  }
}
