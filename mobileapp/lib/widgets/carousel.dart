import "package:flutter/material.dart";
import "package:google_fonts/google_fonts.dart";
import "package:carousel_slider/carousel_slider.dart";
import "package:intl/intl.dart";

import "package:mobileapp/widgets/smallwidgets.dart";

class MainCarousel extends StatefulWidget {
  List<Widget> children;

  MainCarousel({Key? key, required this.children}) : super(key: key);

  @override
  _MainCarouselState createState() => _MainCarouselState();
}

class _MainCarouselState extends State<MainCarousel> {
  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(
        height: 290,
        viewportFraction: 1,
        autoPlay: true,
        autoPlayInterval: const Duration(seconds: 5),
      ),
      items: widget.children,
    );
  }
}

class CarouselCard extends StatelessWidget {
  final String name;
  final String type;
  final void Function() callback;
  final DateTime date;

  CarouselCard(
      {Key? key,
      required this.name,
      required this.date,
      required this.callback,
      this.type = "Project"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(25.0),
      decoration: BoxDecoration(
          // color: const Color.fromRGBO(255, 250, 242, 1),
          color: Colors.orange[50],
          boxShadow: const [
            BoxShadow(
                blurRadius: 3,
                spreadRadius: 2,
                color: Color.fromRGBO(138, 138, 138, 0.1)),
          ],
          borderRadius: BorderRadius.circular(15.0)),
      child: Column(
        children: <Widget>[
          Container(
            child: Text(
              name,
              style: GoogleFonts.rubik(
                color: const Color.fromRGBO(105, 62, 22, 1),
                fontSize: 25,
                fontWeight: FontWeight.w600,
              ),
              textAlign: TextAlign.center,
            ),
            padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
            margin: EdgeInsets.fromLTRB(0, 15.0, 0, 10.0),
          ),
          Divider(
            height: MediaQuery.of(context).size.height * 0.025,
            thickness: 3.0,
            color: Color.fromRGBO(175, 186, 130, 1),
          ),
          Align(
            child: PaddingCard(DetailBeige(type)),
            alignment: Alignment.topLeft,
          ),
          Row(
            children: [
              PaddingCard(
                Row(
                  children: [
                    const Icon(
                      Icons.alarm,
                      color: Color.fromRGBO(52, 30, 11, 1),
                      size: 20.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Text(
                        DateFormat.yMMMMd("en_US").format(date),
                        style: GoogleFonts.rubik(
                          fontSize: 15.0,
                          color: const Color.fromRGBO(52, 30, 11, 1),
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ],
                  mainAxisAlignment: MainAxisAlignment.center,
                ),
              ),
            ],
          ),
          Align(
            alignment: Alignment.topLeft,
            child: PaddingCard(
              ButtonGreenStretch(ButtonText(text: "See more"), callback),
            ),
          ),
        ],
      ),
      width: MediaQuery.of(context).size.width * 0.75,
    );
  }
}
