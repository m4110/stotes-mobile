import "package:flutter/material.dart";
import "package:google_fonts/google_fonts.dart";

// Texts
class Heading extends StatelessWidget {
  final double fontSize;
  final String text;
  TextAlign? textAlign;
  final Color? color;

  Heading(this.text, this.fontSize,
      {this.textAlign = TextAlign.left,
      this.color = const Color.fromRGBO(52, 30, 11, 1)});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        text,
        style: TextStyle(
          fontFamily: "MADESunflower",
          fontSize: fontSize,
          color: color,
        ),
      ),
      margin: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
    );
  }
}

class RubikText extends StatelessWidget {
  final String text;
  final double size;
  TextAlign? textAlign;

  RubikText(this.text, this.size, {this.textAlign = TextAlign.left});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: GoogleFonts.rubik(
        fontSize: size,
        fontWeight: FontWeight.normal,
        color: const Color.fromRGBO(52, 30, 11, 1),
      ),
    );
  }
}

// Buttons
class ButtonGreen extends StatelessWidget {
  final Widget child;
  final void Function() callback;
  final double? fontSize;

  ButtonGreen(this.child, this.callback, {Key? key, this.fontSize = 20.0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: child,
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(
            const Color.fromRGBO(102, 108, 78, 1)),
      ),
      onPressed: callback,
    );
  }
}

class ButtonGreenStretch extends StatelessWidget {
  final Widget child;
  final void Function() callback;
  final double? fontSize;

  ButtonGreenStretch(this.child, this.callback,
      {Key? key, this.fontSize = 20.0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: ButtonGreen(child, callback, fontSize: fontSize),
      width: double.infinity,
    );
  }
}

class ButtonBeige extends StatelessWidget {
  final Widget child;
  final void Function() callback;

  ButtonBeige(this.child, this.callback);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: child,
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(
          const Color.fromRGBO(203, 172, 129, 1),
        ),
      ),
      onPressed: callback,
    );
  }
}

class ButtonRed extends StatelessWidget {
  final Widget child;
  final void Function() callback;

  const ButtonRed({Key? key, required this.child, required this.callback})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: child,
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(
          const Color.fromRGBO(163, 82, 73, 1),
        ),
      ),
      onPressed: callback,
    );
  }
}

class ButtonText extends StatelessWidget {
  const ButtonText({
    Key? key,
    required this.text,
    this.color = Colors.white,
    this.fontSize = 20.0,
  }) : super(key: key);

  final String text;
  final Color? color;
  final double fontSize;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: GoogleFonts.rubik(
        fontSize: fontSize,
        fontWeight: FontWeight.w500,
        color: color,
      ),
    );
  }
}

// Details
class DetailBeige extends StatelessWidget {
  final String text;

  DetailBeige(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Text(
          text,
          style: GoogleFonts.rubik(
            color: Colors.white,
            fontSize: 14.0,
            fontWeight: FontWeight.w500,
          ),
        ),
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.075,
            vertical: 2.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(3.0),
          color: Color.fromRGBO(228, 147, 100, 1),
        ));
  }
}

class DetailBlack extends StatelessWidget {
  final String text;

  DetailBlack(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Text(
          text,
          style: GoogleFonts.rubik(
            color: Colors.white,
            fontSize: 14.0,
            fontWeight: FontWeight.w500,
          ),
        ),
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.075,
            vertical: 2.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(3.0),
          color: Color.fromRGBO(52, 30, 11, 1),
        ));
  }
}

// Paddings
class PaddingCard extends StatelessWidget {
  final Widget child;

  PaddingCard(this.child);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.05, vertical: 2.0),
      child: child,
    );
  }
}

class PaddingContainer extends StatelessWidget {
  final Widget child;
  const PaddingContainer(
    this.child, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: child,
    );
  }
}

// Loading indicators
class LoadingIndicator extends StatefulWidget {
  const LoadingIndicator({Key? key}) : super(key: key);

  @override
  _LoadingIndicatorState createState() => _LoadingIndicatorState();
}

class _LoadingIndicatorState extends State<LoadingIndicator>
    with SingleTickerProviderStateMixin {
  late Animation<Color?> colorTween;
  late AnimationController animationController;

  @override
  void initState() {
    animationController =
        AnimationController(duration: Duration(milliseconds: 900), vsync: this);
    colorTween = animationController.drive(
      ColorTween(
        begin: const Color.fromRGBO(137, 150, 86, 1),
        end: const Color.fromRGBO(105, 62, 22, 1),
      ),
    );
    animationController.repeat();
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Center(
        child:
            CircularProgressIndicator(strokeWidth: 6.0, valueColor: colorTween),
      ),
      height: 150.0,
      width: 150.0,
    );
  }
}

class ButtonLoadingScreen extends StatelessWidget {
  const ButtonLoadingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 16.0,
      width: 16.0,
      child: const Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
          strokeWidth: 3.0,
        ),
      ),
    );
  }
}
