import 'package:flutter/material.dart';
import 'package:mobileapp/model/models.dart';

class SearchBarWidget extends StatefulWidget {
  final String text;
  final ValueChanged<String> onChanged;
  final String hint;


  const SearchBarWidget({
    Key? key,
    required this.text,
    required this.onChanged,
    required this.hint, 
    }) : super(key: key);

  @override
  _SearchBarWidgetState createState() => _SearchBarWidgetState();
}

class _SearchBarWidgetState extends State<SearchBarWidget> {
  final controller = TextEditingController();   // the text field will update the value

  @override
  Widget build(BuildContext context) {
    final styleActive = TextStyle(color: const Color.fromRGBO(52, 30, 11, 1));
    final styleHint = TextStyle(color: const Color.fromRGBO(52, 30, 11, 0.5));
    final style = widget.text.isEmpty ? styleHint : styleActive;

    return Container(
      height: 42,
      margin: const EdgeInsets.fromLTRB(16, 16, 16, 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
        border: Border.all(color: Colors.black26),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: TextField(
        decoration: InputDecoration(
          icon: Icon(Icons.search, color: style.color),
          hintText: widget.hint,
          hintStyle: style,
          border: InputBorder.none,
        ),
        style: style,
        onChanged: widget.onChanged,
      ),
    );
  }
}
