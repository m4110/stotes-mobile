import "package:flutter/material.dart";
import "package:google_fonts/google_fonts.dart";
import 'package:mobileapp/screen/meeting/mymeetings.dart';
import 'package:mobileapp/screen/project/myprojects.dart';
import 'package:mobileapp/screen/project/search_project.dart';
import 'package:mobileapp/screen/project/createproject.dart';

// For logout
import "package:shared_preferences/shared_preferences.dart";
import "package:http/http.dart" as http;
import "package:mobileapp/production.dart";
import "dart:convert";

import 'package:mobileapp/screen/Home.dart';
import "package:mobileapp/screen/account/profile.dart";
import "package:mobileapp/screen/project/specific_projects.dart";
import "package:mobileapp/screen/account/login.dart";

class Navbar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      actions: <Widget>[
        Container(
          child: Tooltip(
            child: ElevatedButton(
              child: const Icon(
                Icons.person,
                size: 20.0,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (_) => ProfileScreen()),
                );
              },
              style: ElevatedButton.styleFrom(
                shape: const CircleBorder(),
                primary: const Color.fromRGBO(137, 150, 86, 1),
                padding: const EdgeInsets.all(5.0),
              ),
            ),
            message: "My Profile",
            verticalOffset: 30,
            decoration: BoxDecoration(
              color: const Color.fromRGBO(181, 190, 142, 1),
              borderRadius: BorderRadius.circular(1.0),
            ),
            textStyle: GoogleFonts.rubik(
              color: Colors.white,
            ),
          ),
          width: 40,
          height: 40,
          padding: const EdgeInsets.only(right: 7.0),
        ),
      ],
      backgroundColor: const Color.fromRGBO(195, 197, 173, 1),
      title: Logo(30),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class Logo extends StatelessWidget {
  final double fontSize;

  Logo(this.fontSize);

  @override
  Widget build(BuildContext context) {
    return Text(
      "Stotes",
      style: TextStyle(
        fontFamily: "MADESunflower",
        fontSize: fontSize,
        color: const Color.fromRGBO(52, 30, 11, 1),
      ),
    );
  }
}

class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        child: ListView(
          children: [
            DrawerHeader(
              child: Padding(
                child: Logo(50.0),
                padding: const EdgeInsets.all(10.0),
              ),
            ),
            const Divider(
              height: 1.5,
              thickness: 1.5,
            ),
            ListTile(
              leading: const Icon(Icons.home),
              title: NavLinks("Home", StotesHome()),
            ),
            ListTile(
                leading: const Icon(Icons.book),
                title: NavLinks("My Projects", MyProjects())),
            ListTile(
              leading: const Icon(Icons.manage_search),
              title: NavLinks("Search Projects", SearchProjects()),
            ),
            ListTile(
              leading: const Icon(Icons.event_note),
              title: NavLinks("Meetings", MyMeetings()),
            ),
          ],
        ),
        color: const Color.fromRGBO(181, 190, 142, 1),
      ),
    );
  }
}

class NavLinks extends StatelessWidget {
  final String text;
  final Widget route;

  NavLinks(this.text, this.route);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: TextButton(
        child: Text(text,
            style: GoogleFonts.rubik(
              color: const Color.fromRGBO(90, 97, 56, 1),
              fontSize: 22.0,
            ),
            textAlign: TextAlign.left),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (_) => route),
          );
        },
      ),
    );
  }
}
