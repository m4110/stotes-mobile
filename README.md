# stotesmobile

[![Test and Deploy][actions-badge]][commits-gh]
[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

# Project PBP I02 Group Assignment


## Group members:
- Indira Devi Rusvandy (2006488732)
- Benedictus Jevan Winata (2006519870)
- Bryant Tanujaya (2006519896)
- Alexander Caesario Bangun (2006519851)
- Ajie Restu Sanggusti (2006490081)
- Muhammad Aaqil Abdullah (2006489501)
- Nicholas Jonathan Kinandana (2006490106)

## Project Description:

Stotes Mobile is a workspace mobile application that helps organizing your daily tasks and makes sure
your remote work gets finished on time. Create projects and invite your team members to keep you guys all on the same page. 

Users can sign in to their accounts, or register a new one. Then, they will be directed to the homepage where they can either start a new project, or look at some of their recent projects and tasks. To boost productivity, there is a newsfeed centering around how to be more productive. 

When a user creates a new project, they can opt to add others to join as members of the workspace, as well as setting up a timeframe when the project is due. Next comes the fun part—tasks! Creators of the workspace can start adding tasks with their: deadline, PIC, type, and description. Creators can also create meetings, that state the date and time as well as the link to the meeting.

Users can also join existing projects as members, simply by searching the projectID and inputing the passcode. 



## Modules:
1.  Home: 
    This is the landing page for Stotes, which will display only a newsfeed as well as a prompt to sign in or register for non-signed in users. For signed in users, it will display the newsfeed, that is fetched from an API. It also shows at most their three last active projects and all of their recent active tasks that's fetched from endpoint in the Django views.
2.  Accounts:
    This app serves as the authentication for non-signed in users. Users can login and logout. It will post data inputted in the forms which will accepted by the endpoint in the views. 
3.  Create Project:
    This is where signed in users can finally create their projects, starting by filling out a form: name of project, members (optional), description, passcode, and timeframe of the project. These information will be posted and passed before being fetched by the endpoint in the views.
4.  Join Project
    Signed in users can join projects by searching up the ID of the project from the landing page, and entering the project passcode to start working on the project. The ID will be posted and fetched by the endpoint in the views to be verified afterwards.
5.  Project Workspace
    The creator of the project can start to add/delete tasks for the members to see, as well as edit previously made tasks. It will fetch the data of project and task from endpoint to be displayed. It will also receive data from endpoint in views for create or deletion of tasks.
6.  Task
    Tasks can be viewed by both the creators and members. But only the creator can edit/delete the following atttributes: due date, type, description, and PIC. It will fetch the data of task from endpoint to be displayed. It will also receive data from endpoint in views for create or deletion of tasks.
7.  Meeting
    Meetings can be set up by anyone in the workspace, and they have the following attributes: date & time, description, person in meeting, meeting URL. It will fetch the data of meeting from endpoint to be displayed. It will also receive data from endpoint in views for create or deletion of meeting. 


## APK
[Link](https://www.dropbox.com/s/ncp3xos2ywlgqyv/stotes-mobile.apk?dl=0)


[actions-badge]: https://github.com/laymonage/django-template-heroku/workflows/Test%20and%20Deploy/badge.svg
[commits-gh]: https://github.com/laymonage/django-template-heroku/commits/master
[pipeline-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/laymonage/django-template-heroku/-/commits/master
[readme-en]: README.en.md
[heroku-dashboard]: https://dashboard.heroku.com
[djecrety]: https://djecrety.ir
[account-settings]: https://dashboard.heroku.com/account
[chromedriver]: https://chromedriver.chromium.org/downloads
[homebrew]: https://brew.sh
[ticket-21227]: https://code.djangoproject.com/ticket/21227
[bypass-cache]: https://en.wikipedia.org/wiki/Wikipedia:Bypass_your_cache
[flake8]: https://pypi.org/project/flake8
[pylint]: https://pypi.org/project/pylint
[black]: https://pypi.org/project/black
[isort]: https://pypi.org/project/isort
[template]: https://docs.djangoproject.com/en/3.2/ref/django-admin/#cmdoption-startproject-template
[repo-gh]: https://github.com/laymonage/django-template-heroku
[repo-gl]: https://gitlab.com/laymonage/django-template-heroku
[license]: LICENSE
