from django.urls import path, include
from .views import loginHandler, getUser, logoutHandler, register

urlpatterns = [
    path("login/", loginHandler, name="loginHandler"),
    path("logout/", logoutHandler, name="logoutHandler"),
    path("register/", register, name="register"),
    path("getuser/", getUser, name="getUser"),
]