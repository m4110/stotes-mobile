from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.http.response import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import model_to_dict
import json

from django.conf import settings
from importlib import import_module

# Create your views here.
@csrf_exempt
def register(request):
    if request.method == "POST":
        requestBody = json.loads(request.body.decode("utf-8"))
        username = requestBody["username"]
        name = requestBody["name"]
        email = requestBody["email"]
        password = requestBody["password"]


        firstName = " ".join(name.split()[:-1]) if len(name.split()) > 1 else name.split()[0]
        lastName = name.split()[-1]

        userWithSimilarUsername = User.objects.filter(username=username)
        if userWithSimilarUsername:
            return HttpResponse("Duplicate User", status=409)
        
        userWithSimilarEmail = User.objects.filter(email=email)
        if userWithSimilarEmail:
            return HttpResponse("Duplicate Email", status=409)

        User.objects.create_user(username=username, first_name=firstName, last_name=lastName, email=email, password=password)

        return HttpResponse("OK", status=201)

@csrf_exempt
def loginHandler(request):
    if request.method == "POST": 
        requestBody = json.loads(request.body.decode("utf-8"))
        username = requestBody["username"]
        inputtedPassword = requestBody["password"]
        

        user = authenticate(request, username=username, password=inputtedPassword)
        if user is not None:
        
            login(request, user)
            return JsonResponse(
                {
                    "sessionId" : request.session.session_key
                }
            )
        
        return HttpResponse("Unauthorized", status=401)
    return HttpResponse("Not Found",status = 404)

@csrf_exempt
def logoutHandler(request):
    if request.method == "POST":
        requestBody = json.loads(request.body.decode("utf-8"))
        sessionId = requestBody["sessionid"]

        engine = import_module(settings.SESSION_ENGINE)
        sessionstore = engine.SessionStore
        session = sessionstore(sessionId)

        session.flush()
        return HttpResponse("OK", status=200)

@csrf_exempt
def getUser(request):
    
    sessionId = request.headers["Sessionid"]

    engine = import_module(settings.SESSION_ENGINE)
    sessionstore = engine.SessionStore
    session = sessionstore(sessionId)
    userId = session.get("_auth_user_id")
    user = User.objects.get(id = userId)
    # print(model_to_dict(user))

    if request.method == "POST":
        body = json.loads(request.body.decode("utf-8"))
        firstName = body["firstName"]
        lastName = body["lastName"]

        user.first_name = firstName
        user.last_name = lastName
        user.save()

        return HttpResponse("OK", status=204)
    

    return JsonResponse(model_to_dict(user), safe = False)


