from django.urls import path, include
from .views import addMeeting, currentProject, addTask

urlpatterns = [
    path("<int:pk>/", currentProject, name="currentProject"),
    path("<int:projectId>/addTask/", addTask, name="addTask"),
    path("<int:projectId>/addMeeting/", addMeeting, name="addMeeting"),

]