from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse_lazy
import datetime

# Create your models here.
class Project(models.Model):
    name = models.CharField(max_length = 30)
    description = models.TextField()
    passcode = models.CharField(max_length = 20)
    startDate = models.DateField(auto_now_add = True)
    endDate = models.DateField()
    creator = models.ForeignKey(User, on_delete = models.CASCADE, related_name = "project")
    projectParticipants = models.ManyToManyField(User, related_name = "projects")
    lastOpened = models.DateTimeField(auto_now_add = True)

    def get_absolute_url(self):
        return reverse_lazy('current_project', kwargs={'project_id': self.id})
    
    def __str__(self):
        return self.name

    @property
    def remaining_days(self):
        remaining = (self.endDate - datetime.datetime.now().date()).days
        return remaining