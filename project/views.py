from django.shortcuts import render
from django.http.response import JsonResponse, HttpResponse
from django.core import serializers
from django.forms.models import model_to_dict
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User

from .models import Project
from task.models import Task
from meeting.models import Meeting


from importlib import import_module
from django.conf import settings

import json
import datetime

# Create your views here.
def currentProject(request, pk):
    context = {}

    project = Project.objects.filter(id=pk)
    context["chosenProject"] = serializers.serialize("json", project)
    
    tasks = Task.objects.filter(project = project[0])
    context["taskLst"] = serializers.serialize("json", tasks)

    meetings = Meeting.objects.filter(project = project[0], dateAndTime__gt=datetime.datetime.now() - datetime.timedelta(days=1.5)).order_by("dateAndTime")
    context["meetingList"] = serializers.serialize("json", meetings)


    return JsonResponse(context, safe=False)

@csrf_exempt
def addTask(request, projectId):

    sessionId = request.headers["Sessionid"]
    
    engine = import_module(settings.SESSION_ENGINE)
    sessionstore = engine.SessionStore
    session = sessionstore(sessionId)
    userId = session.get("_auth_user_id")
    user = User.objects.get(id = userId)
    
    project = Project.objects.get(id = projectId)
    
    participants = serializers.serialize("json", project.projectParticipants.all())

    if request.method == "POST":
        body = json.loads(request.body.decode("utf-8"))
        
        name = body["name"]
        description = body["description"]
        taskType = body["type"]
        dueDateString = [int(string) for string in body["dueDate"].split(" ")[0].split("-")]
        dueDate = datetime.date(dueDateString[0], dueDateString[1], dueDateString[2])

        picIds = [int(id) for id in json.loads(body["participants"])]
        peopleInCharge = [User.objects.get(id=id) for id in picIds]
        
        task = Task.objects.create(name=name, description=description, taskType = taskType, dueDate=dueDate, project=project, creator=user)
        task.save()

        for person in peopleInCharge:
            task.peopleInCharge.add(person)

        return HttpResponse("Created", status=201)

    return JsonResponse(participants, safe=False)

@csrf_exempt
def addMeeting(request, projectId):

    sessionId = request.headers["Sessionid"]
    
    engine = import_module(settings.SESSION_ENGINE)
    sessionstore = engine.SessionStore
    session = sessionstore(sessionId)
    userId = session.get("_auth_user_id")
    user = User.objects.get(id = userId)
    
    project = Project.objects.get(id = projectId)
    
    participants = serializers.serialize("json", project.projectParticipants.all())

    if request.method == "POST":
        body = json.loads(request.body.decode("utf-8"))
        
        name = body["name"]
        description = body["description"]
        link = body["link"]
        dateTimeString = [int(string) for string in body["dateTime"].split(" ")[0].split("-")]
        dateTime = datetime.datetime(dateTimeString[0], dateTimeString[1], dateTimeString[2], int(body["dateTime"][11:13]), int(body["dateTime"][14:16]), int(body["dateTime"][17:19]))

        participantIds = [int(id) for id in json.loads(body["participants"])]
        meetingParticipants = [User.objects.get(id=id) for id in participantIds]
        
        meeting = Meeting.objects.create(name=name, description=description, link = link, dateAndTime=dateTime, project=project, creator=user)
        meeting.save()

        for person in meetingParticipants:
            meeting.meetingParticipants.add(person)

        return HttpResponse("Created", status=201)

    return JsonResponse(participants, safe=False)
