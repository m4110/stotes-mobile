from django.apps import AppConfig


class JoinProjectConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'join_project'
