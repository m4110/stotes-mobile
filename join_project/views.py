

# Create your views here.
import json
from django.db.models import query
from django.shortcuts import render
from django.http.response import HttpResponse, JsonResponse
from django.core import serializers
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from project.models import Project


from django.conf import settings
from importlib import import_module


def search_project(request):
    context = {}

    sessionId = request.headers["sessionId"]
    query = request.headers["query"]
    pageNum = int(request.headers["pageNum"])
    
    engine = import_module(settings.SESSION_ENGINE)
    sessionstore = engine.SessionStore
    session = sessionstore(sessionId)
    userId = session.get("_auth_user_id")
    user = User.objects.get(id = userId)

    project = []

    if query == '':
        for i in Project.objects.order_by('name'):
            if user not in i.projectParticipants.all():
                project.append(i)
    else:
        for i in Project.objects.filter(name__icontains=query).order_by('name'):
            if user not in i.projectParticipants.all():
                project.append(i)
    
    projectSlice = project[(pageNum * 10):((pageNum * 10) + 10)]

    allProjects = serializers.serialize("json", projectSlice)
    context["projects"] = allProjects

    return JsonResponse(context, safe=False)

def has_more(request):
    context = {}

    sessionId = request.headers["sessionId"]
    query = request.headers["query"]
    pageNum = int(request.headers["pageNum"])
    
    engine = import_module(settings.SESSION_ENGINE)
    sessionstore = engine.SessionStore
    session = sessionstore(sessionId)
    userId = session.get("_auth_user_id")
    user = User.objects.get(id = userId)

    project = []

    if query == '':
        for i in Project.objects.order_by('name'):
            if user not in i.projectParticipants.all():
                project.append(i)
    else:
        for i in Project.objects.filter(name__icontains=query).order_by('name'):
            if user not in i.projectParticipants.all():
                project.append(i)
    
    projectSlice = project[(pageNum * 10):((pageNum * 10) + 10)]

    if projectSlice == []:
        return HttpResponse("Failed", status = 500)

    else:
        allProjects = serializers.serialize("json", projectSlice)
        context["projects"] = allProjects
        return JsonResponse(context, safe=False) 

@csrf_exempt
def join_project(request, pk):
    sessionId = request.headers["Sessionid"]
    
    engine = import_module(settings.SESSION_ENGINE)
    sessionstore = engine.SessionStore
    session = sessionstore(sessionId)
    userId = session.get("_auth_user_id")
    user = User.objects.get(id = userId)

    if request.method == "POST":
        body = json.loads(request.body.decode("utf-8"))
        passcode = body["passcode"]
        
        project = project = Project.objects.get(id = pk)
        
        if project.passcode == passcode:
            project.projectParticipants.add(user)
            return JsonResponse({"id":project.id})

    return HttpResponse("Failed", status = 500)

