from django.urls import path

from . import views


app_name = 'join_project'

urlpatterns = [
    path("project/join/<int:pk>/", views.join_project, name="join_project"),
    path("search-projects/", views.search_project, name="search_project"),
    path("has-more/", views.has_more, name="has_more"),
]