from django.shortcuts import render
from django.http.response import HttpResponse, JsonResponse
from django.core import serializers
from django.contrib.auth.models import User
from project.models import Project
from task.models import Task
from meeting.models import Meeting


from django.conf import settings
from importlib import import_module
import datetime


def page(request):
    context = {}

    sessionId = request.headers["Sessionid"]
    
    engine = import_module(settings.SESSION_ENGINE)
    sessionstore = engine.SessionStore
    session = sessionstore(sessionId)
    userId = session.get("_auth_user_id")
    user = User.objects.get(id = userId)

    project = []
    task = []

    counter = 0
    for i in Project.objects.order_by('-lastOpened'):
        if user in i.projectParticipants.all():
            project.append(i)
            if counter > 1:
                break
            counter += 1
            
    counter = 0
    for i in Task.objects.order_by('-dueDate'):
        
        if user in i.peopleInCharge.all():
            task.append(i)
            if counter > 3:
                break
            counter += 1

    allProjects = serializers.serialize("json", project)
    context["projects"] = allProjects

    allTasks = serializers.serialize("json", task)
    context["tasks"] = allTasks
    


    return JsonResponse(context, safe=False)

def home(request):
    return render(request, "main/home.html", {})

def my_projects(request):
    context = {}

    sessionId = request.headers["Sessionid"]
    query = request.headers["query"]
    pageNum = int(request.headers["pageNum"])
    
    engine = import_module(settings.SESSION_ENGINE)
    sessionstore = engine.SessionStore
    session = sessionstore(sessionId)
    userId = session.get("_auth_user_id")
    user = User.objects.get(id = userId)

    project = []

    if query == '':
        for i in Project.objects.order_by('-lastOpened'):
            if user in i.projectParticipants.all():
                project.append(i)
    else:
        for i in Project.objects.filter(name__icontains=query).order_by('name'):
            if user in i.projectParticipants.all():
                project.append(i)

    projectSlice = project[(pageNum * 10):((pageNum * 10) + 10)]

    allProjects = serializers.serialize("json", projectSlice)
    context["projects"] = allProjects

    return JsonResponse(context, safe=False)

def has_more(request):
    context = {}

    sessionId = request.headers["sessionId"]
    query = request.headers["query"]
    pageNum = int(request.headers["pageNum"])
    
    engine = import_module(settings.SESSION_ENGINE)
    sessionstore = engine.SessionStore
    session = sessionstore(sessionId)
    userId = session.get("_auth_user_id")
    user = User.objects.get(id = userId)

    project = []

    if query == '':
        for i in Project.objects.order_by('-lastOpened'):
            if user in i.projectParticipants.all():
                project.append(i)
    else:
        for i in Project.objects.filter(name__icontains=query).order_by('name'):
            if user in i.projectParticipants.all():
                project.append(i)
    
    projectSlice = project[(pageNum * 10):((pageNum * 10) + 10)]

    if projectSlice == []:
        return HttpResponse("Failed", status = 500)

    else:
        allProjects = serializers.serialize("json", projectSlice)
        context["projects"] = allProjects
        return JsonResponse(context, safe=False)  

def my_meetings(request):
    context = {}

    sessionId = request.headers["Sessionid"]
    
    engine = import_module(settings.SESSION_ENGINE)
    sessionstore = engine.SessionStore
    session = sessionstore(sessionId)
    userId = session.get("_auth_user_id")
    user = User.objects.get(id = userId)

    meetings = []

    for i in Meeting.objects.filter(dateAndTime__gt=datetime.datetime.now() - datetime.timedelta(days=1.5)).order_by('dateAndTime'):
        if user in i.meetingParticipants.all():
            meetings.append(i)
    

    allMeetings = serializers.serialize("json", meetings)
    context["meetings"] = allMeetings

    return JsonResponse(context, safe=False)
