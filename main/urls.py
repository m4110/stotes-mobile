from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('home/', views.page, name='page'),
    path("", views.home, name="home"),
    path('my-projects/', views.my_projects, name='my_projects'),
    path('my-meetings/', views.my_meetings, name='my_meetings'),
    path('my-has-more/', views.has_more, name='has_more'),

]
