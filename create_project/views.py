from django.shortcuts import render
from django.http.response import JsonResponse, HttpResponse
from django.core import serializers
from django.forms.models import model_to_dict
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User

from project.models import Project
from task.models import Task

from importlib import import_module
from django.conf import settings

import json
import datetime

# Create your views here.
@csrf_exempt
def createProjects(request):
    sessionId = request.headers["Sessionid"]
    
    engine = import_module(settings.SESSION_ENGINE)
    sessionstore = engine.SessionStore
    session = sessionstore(sessionId)
    userId = session.get("_auth_user_id")
    user = User.objects.get(id = userId)

    if request.method == "POST":
        body = json.loads(request.body.decode("utf-8"))
        name = body["name"]
        description = body["description"]
        passcode = body["passcode"]
        endDateString = [int(string) for string in body["endDate"].split(" ")[0].split("-")]
        endDate = datetime.date(endDateString[0], endDateString[1], endDateString[2])
        
        project = Project.objects.create(name = name,description = description,passcode = passcode,endDate = endDate,creator = user)
        project.save()
        return JsonResponse({"id":project.id})

    return HttpResponse("Failed", status = 500)



