from django.db import models
from project.models import Project
from django.contrib.auth.models import User
import datetime

# Create your models here.
class Task(models.Model):

    TYPES = (
        ("Presentation", "Presentation"),
        ("Report", "Report"),
        ("Design", "Design"),
        ("Social Media", "Social Media"),
        ("Assignment", "Assignment"),
        ("Paper", "Paper"),
    )

    name = models.CharField(max_length = 30, null = False)
    description = models.TextField()
    taskType = models.CharField(max_length = 30, choices = TYPES, null = False)
    dueDate = models.DateField()

    project = models.ForeignKey(Project, on_delete = models.CASCADE, related_name = "task")
    creator = models.ForeignKey(User, on_delete = models.CASCADE, related_name = "task_creator")
    peopleInCharge = models.ManyToManyField(User, related_name = "tasks")

    @property
    def remaining_days(self):
        return (self.dueDate - datetime.datetime.now().date()).days

    def __str__(self):
        return self.name