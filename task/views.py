from django.core import serializers
from django.core.serializers import serialize
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from .models import Task

import json
import datetime

# Create your views here.
@csrf_exempt
def deleteTask(request, id):
    if request.method == "DELETE":
        task = Task.objects.get(id=id)
        
        if task is None:
            return HttpResponse("Not found", status=404)

        task.delete()
        return HttpResponse("Deleted", status=200)
        
@csrf_exempt
def editTask(request, id):
    context = {}
    task = Task.objects.filter(id = id)
    context["task"] = serializers.serialize("json", task)
    context["peopleInCharge"] = serializers.serialize("json", task[0].peopleInCharge.all())
    
    
    project = task[0].project
    context["participants"] =  serializers.serialize("json", project.projectParticipants.all())
    
    if request.method == "POST":
        body = json.loads(request.body.decode("utf-8"))
        
        name = body["name"]
        description = body["description"]
        taskType = body["type"]
        dueDateString = [int(string) for string in body["dueDate"].split(" ")[0].split("-")]
        dueDate = datetime.date(dueDateString[0], dueDateString[1], dueDateString[2])

        picIds = [int(id) for id in json.loads(body["participants"])]
        peopleInCharge = [User.objects.get(id=id) for id in picIds]

        task[0].name = name
        task[0].description = description
        task[0].taskType = taskType
        task[0].dueDate = dueDate
        task[0].save()

        for user in set(peopleInCharge):
            task[0].peopleInCharge.add(user)
        
        return HttpResponse("Updated", status=204)


    return JsonResponse(context, safe=False)

def currentTask(request, id):
    context = {}

    task = Task.objects.filter(id = id)
    context["chosenTask"] = serializers.serialize("json", task)
    listPeople = task[0].peopleInCharge.all()[:3]
    peopleInCharge = serializers.serialize("json", listPeople)
    context["peopleInCharge"] = peopleInCharge

    return JsonResponse(context, safe=False)

