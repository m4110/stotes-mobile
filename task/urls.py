from django.urls import path
from .views import currentTask, deleteTask, editTask

urlpatterns = [
    path("delete/<int:id>/", deleteTask, name="deleteTask"),
    path("edit/<int:id>/", editTask, name="editTask"),
    path("<int:id>", currentTask, name="currentTask")
]